//
//  EnseignementDetailViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 11/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress/KVNProgress.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <AFNetworking/AFNetworking.h>

@interface EnseignementDetailViewController : UIViewController<UIScrollViewDelegate>

@property NSString* Id;
@property NSURL* thumbnail;
@property CGFloat lastContentOffSet;
@property CGFloat oldHeight;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *publish_date;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;
@property (strong, nonatomic) IBOutlet FBSDKLikeButton *fb_article_link;
@property (weak, nonatomic) IBOutlet UILabel *titre;

@property (weak, nonatomic) IBOutlet UILabel *content;

@end
