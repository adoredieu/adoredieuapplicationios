//
//  EnseignementViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 11/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnseignementViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *EnseignementsItems;
@property CATransform3D animation;
@property int lastContentOffset;
+ (int) page;
- (void)addPage;
-(void)loadMoreArticle:(int)page;

@end
