//
//  DailyVerseViewController.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 28/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "DailyVerseViewController.h"
#import "AFHTTPSessionManager.h"

@interface DailyVerseViewController ()

@end

@implementation DailyVerseViewController

- (void)viewDidLoad {

    // Do any additional setup after loading the view.
    NSString* url = @"http://www.adoredieu.com/mobility/daily_verse.php";
    
    /*
     Requete JSON, et parsing de la réponse json reçue.
     */
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSString* response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
             [self.verseUI setText:response];
         }
         failure:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
         }];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
