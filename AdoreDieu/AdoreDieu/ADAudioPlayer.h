//
//  ADAudioPlayer.h
//  AdoreDieu
//
//  Created by Jeremy on 19/12/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataService.h"
#import "AFNetworking.h"
#import <AVFoundation/AVFoundation.h>

@interface ADAudioPlayer : NSObject

@property BOOL isPlaying;
@property NSIndexPath* selectedRow;
@property (strong, nonatomic) AVPlayer *audioPlayer;

- (void)timeToSeek:(UISlider*) currentTimeAudioSlider;

- (void)setSong:(NSInteger) row musiques:(NSMutableArray *) songs songNameLabel:(UILabel *) songName artisteLabel:(UILabel *) artiste albumCoverImageView:(UIImageView *) albumCover;
- (void)playSong:(UIButton *) playButton;
- (void)audioPlayerDidFinishPlaying:(AVPlayer *)player successfully:(BOOL)flag;
- (void)audioPlayerDecodeErrorDidOccur:(AVPlayer *)player error:(NSError *)error;
+ (ADAudioPlayer*)sharedAudioPlayer;
- (void) configureAVAudioPlayer;

@end
