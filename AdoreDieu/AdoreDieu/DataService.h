//
//  DataService.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 22/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataService : NSObject
@property (strong, nonatomic) NSUserDefaults* myDataService;
+ (DataService*) sharedDataService;
- (BOOL)canConnectToWeb ;
@end
