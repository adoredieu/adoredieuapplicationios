//
//  DBTUtils.m
//  AdoreDieu
//
//  Created by Jeremy on 06/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "DBTUtils.h"

@implementation DBTUtils

+(NSMutableArray<Verset*>*)loadVersesDamId:(NSString *)damId book:(NSString *)bookId chapter:(NSNumber *)chapterId textView:(UITextView*)textView
{
    NSMutableArray<Verset*>* allVerses = [[NSMutableArray alloc] init];
    if(![[DataService sharedDataService] canConnectToWeb] &&
       ([[AncienTestament getAnciensTestamentsByDamId:damId] count] > 0 ||
        [[NouveauTestament getNouveauxTestamentsByDamId:damId] count] > 0))
    {
        for (Verset* v in [[Chapitre getChapitreByIndex:[chapterId intValue] withDamId:damId andBookId:bookId] versets]) {
            [allVerses addObject:v];
        };
    }
    else{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [DBT getTextVerseWithDamId:damId
                              book:bookId
                           chapter:chapterId
                        verseStart:nil
                          verseEnd:nil
                           success:^(NSArray *verses) {
                               for(DBTVerse* verse in verses)
                               {
                                   Verset* v = [[Verset alloc] init];
                                   v.verset = verse.verseText;
                                   v.index = [verse.verseId intValue];
                                   [allVerses addObject:v];
                               }
                               dispatch_semaphore_signal(sema);
                           } failure:^(NSError *error)
         {
             NSLog(@"Error: %@", error);
         }];
        while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];
        }
    }
    return allVerses;
}

+(void)loadVerses:(NSString *)damId book:(NSString *)bookId chapter:(NSNumber *)chapterId textView:(UITextView*)textView{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableString* gVerse = [[NSMutableString alloc] init];
        NSMutableArray<Verset*>* allVerses = [self loadVersesDamId:damId book:bookId chapter:chapterId textView:textView];
        gVerse = [NSMutableString stringWithString:@""];
        int i = 1;
        for (Verset* v in allVerses) {
            [gVerse appendFormat:@" (%d) ",i];
            [gVerse appendFormat:@" %@ \n",v.verset];
            i++;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            textView.text = gVerse;
        });
    });
}

+(NSMutableArray<Chapitre*>*)loadChaptersWithDamId:(NSString *)damId bookId:(NSString *)bookId
{
    //BOOL c1 =[[AncienTestament getAnciensTestamentsByDamId:damId] count] > 0;
    //BOOL c2 = [[AncienTestament getAnciensTestamentsByDamId:damId] count] > 0;
    NSMutableArray<Chapitre*>* chapitres = [[NSMutableArray alloc] init];
    if(![[DataService sharedDataService] canConnectToWeb] &&
       ([[AncienTestament getAnciensTestamentsByDamId:damId] count] > 0 ||
        [[NouveauTestament getNouveauxTestamentsByDamId:damId] count] > 0))
    {
        for (Chapitre* c in [[Livre getLivreByBookId:bookId andDamId:damId] chapitres]) {
            [chapitres addObject:c];
        };
        return chapitres;
    }
    else{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [DBT getLibraryChapterWithDamId:damId
                                 bookId:bookId
                                success:^(NSArray *libraryBookNames) {
                                    NSLog(@"Thread : %@", [NSThread currentThread]);
                                        for(DBTChapter *chapter in libraryBookNames)
                                        {
                                            Chapitre* c = [[Chapitre alloc] init];
                                            c.bookId = chapter.bookId;
                                            c.damId = chapter.damId;
                                            c.chapterName = chapter.chapterName;
                                            c.index = [chapter.chapterId intValue];
                                            [chapitres addObject:c];
                                        }
                                    dispatch_semaphore_signal(sema);
                                } failure:^(NSError *error) {
                                    NSLog(@"Error: %@",error);
                                }];
        
        while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];
        }
        return chapitres;
    }
}

@end
