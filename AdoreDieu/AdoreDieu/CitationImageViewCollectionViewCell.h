//
//  CitationImageViewCellCollectionViewCell.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 11/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface CitationImageViewCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *publish_date;
@property (strong, nonatomic) IBOutlet FBSDKLikeControl *like_control;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnail;
@end
