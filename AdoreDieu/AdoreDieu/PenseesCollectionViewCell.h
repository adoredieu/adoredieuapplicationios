//
//  CitationCollectionViewCell.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 21/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PenseesCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnail;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *publish_date;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;
@property (weak, nonatomic) IBOutlet UILabel *introduction;

@end
