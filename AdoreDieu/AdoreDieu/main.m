//
//  main.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 13/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
