//
//  EnseignementCollectionViewCell.h
//  AdoreDieu
//
//  Created by Jeremy on 11/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnseignementCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;
@property (weak, nonatomic) IBOutlet UILabel *titre;

@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *publish_date;
@property (weak, nonatomic) IBOutlet UILabel *introtext;
@end
