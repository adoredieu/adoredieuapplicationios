//
//  Versets.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>

@interface Verset : RLMObject

@property NSString *bookId;
@property NSString* damId;
@property NSString *bookName;
@property int bookOrder;
@property int chapterId;
@property NSString *chapterTitle;
@property int paragraphNumber;
@property NSString* verset;
@property int index;

@end
RLM_ARRAY_TYPE(Verset)