//
//  DailyVerseViewController.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 28/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailyVerseViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *verseUI;

@end
