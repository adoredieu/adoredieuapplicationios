//
//  BibleTableViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 13/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress/KVNProgress.h>

@interface BibleTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet KVNProgress *circularProgressView;

@property NSMutableArray *Bibles;
- (void) callContinuation;
@end
