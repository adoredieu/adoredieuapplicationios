//
//  MenuCollectionViewController.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 20/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface MenuCollectionViewController : REFrostedViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *Menu;

@property (strong, nonatomic) NSMutableArray *MenuItems;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
