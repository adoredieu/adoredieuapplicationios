//
//  MenuCollectionViewController.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 20/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "MenuCollectionViewController.h"
#import "MenuItemCollectionViewCell.h"
#import "PenseesCollectionView.h"
#import "DailyCitationViewController.h"
#import "DailyVerseViewController.h"
#import "CitationImageViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "DEMONavigationController.h"
#import "MenuItem.h"

@interface MenuCollectionViewController ()

@end

@implementation MenuCollectionViewController

static NSString * const reuseIdentifier = @"MenuItemCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Accueil";
    //Initialisation de la navigationBar
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
                                                 style:UIBarButtonItemStylePlain
                                                target:(DEMONavigationController *)self.navigationController
                                                action:@selector(showMenu)];
    
    NSString* bundleID = [[NSBundle mainBundle] bundleIdentifier];
    
    //Création du contenu du menu
    //NSMutableArray *dict = [[NSMutableArray alloc]init];
    
    //Ajout d'un bouton :[dict setObject:[UIImage imageNamed:@"NOM_DE_L'IMAGE"] forKey:@"]
    /*[dict addObject:[[MenuItem alloc] initWithName:@"Verset du jour" imgName:@"ic_daily_verse"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Pensée du jour" imgName:@"ic_pensee"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Citations en image" imgName:@"ic_citation_image"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Citation du jour" imgName:@"ic_citation"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Lecteur audio" imgName:@"ic_audio_player"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"La Bible" imgName:@"ic_bible_big"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Enseignements" imgName:@"ic_enseignements"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Communauté" imgName:@"ic_communities"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Le Site" imgName:@"ic_site_web"]];
    [dict addObject:[[MenuItem alloc] initWithName:@"Paramètres" imgName:@"gears"]];
    
    [FBSDKSettings setAppID:@"464268983747667"];
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    [self.view addSubview:loginButton];
    CGRect frame = loginButton.frame;
    
    //align on top right
    CGFloat xPosition = CGRectGetWidth(loginButton.frame)/1.3;
    CGFloat yPosition = CGRectGetHeight(loginButton.frame)/1.1;
    frame.origin = CGPointMake(ceil(xPosition), yPosition);
    loginButton.frame = frame;
    
    self.MenuItems = dict;*/
}

- (void)awakeFromNib
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>
/*
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.MenuItems.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MenuItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    //Configuration de la cellule
    long row = [indexPath row];    
    cell.MenuLabel.text = [[self.MenuItems objectAtIndex:row] name];
    [cell.MenuImage setImage:[UIImage imageNamed:[[self.MenuItems objectAtIndex:row] imgName]]];
    
    return cell;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    long row = [indexPath row];
    NSString* key = [[self.MenuItems objectAtIndex:row] name];
    
    //Redirection vers le controller souhaité en fonction de l'item selectionné
    if([key  isEqual: @"Pensée du jour"]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PenseesCollectionView *viewController = (PenseesCollectionView *)[storyboard instantiateViewControllerWithIdentifier:@"Pensée du jour"];
            [self presentViewController:viewController animated:YES completion:nil];
    }
    if([key  isEqual: @"Citation du jour"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DailyCitationViewController *viewController = (DailyCitationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Citation du jour"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
    if([key  isEqual: @"Verset du jour"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DailyVerseViewController *viewController = (DailyVerseViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Verset du jour"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
    if([key  isEqual: @"Citations en image"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CitationImageViewController *viewController = (CitationImageViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Citations en image"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

#pragma mark <UICollectionViewDelegate>


// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
