//
//  ADAudioPlayer.m
//  AdoreDieu
//
//  Created by Jeremy on 19/12/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "ADAudioPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation ADAudioPlayer


- (void)timeToSeek:(UISlider*) currentTimeAudioSlider{
    CMTime newTime = CMTimeMakeWithSeconds(currentTimeAudioSlider.value, 1);
    [self.audioPlayer seekToTime:newTime];
    [self.audioPlayer play];
}

- (void)setSong:(NSInteger) row musiques:(NSMutableArray *) songs songNameLabel:(UILabel *) songName artisteLabel:(UILabel *) artiste albumCoverImageView:(UIImageView *) albumCover{
    NSMutableArray* zik = [songs objectAtIndex:row];
    NSDictionary* zik_artiste = zik[0];
    NSDictionary* zik_title = zik[1];
    NSDictionary* zik_cover = zik[2];
    NSDictionary* zik_file = zik[3];
    
    NSString* movieUrl = [zik_file objectForKey:@"file"];
    NSURL *movieURL = [NSURL URLWithString:movieUrl];
    
    //Image de l'album séléctionné
    NSString* imgUrl = [zik_cover objectForKey:@"cover"];
    NSURL *imageURL = [NSURL URLWithString:imgUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    [albumCover setImage:[UIImage imageWithData:imageData]];
    
    songName.text = [zik_title objectForKey:@"title"];
    artiste.text = [zik_artiste objectForKey:@"artiste"];
    NSLog(@"Url : %@",movieURL);
    self.audioPlayer = [[AVPlayer alloc] initWithURL:movieURL];
    
    if ([MPNowPlayingInfoCenter class])  {
        /* we're on iOS 5, so set up the now playing center */
        
        UIImage *albumArtImage = [UIImage imageWithData:imageData];
        MPMediaItemArtwork* albumArt = [[MPMediaItemArtwork alloc] initWithImage:albumArtImage];
        
        NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterNoStyle;
        
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        
        /*NSDictionary *info = @{
                                   MPMediaItemPropertyTitle: [zik_title objectForKey:@"title"],
                                   MPMediaItemPropertyArtist: [zik_artiste objectForKey:@"artiste"] ,
                                   MPMediaItemPropertyGenre: @"Worship",
                                   MPMediaItemPropertyArtwork: albumArt
                               };
    
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:info];*/
        
        [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = @{
                                                                  MPMediaItemPropertyTitle : [zik_title objectForKey:@"title"],
                                                                  MPMediaItemPropertyArtist : [zik_artiste objectForKey:@"artiste"],
                                                                  MPMediaItemPropertyArtwork : albumArt,
                                                                  MPNowPlayingInfoPropertyPlaybackRate : @1.0f
                                                                  };
    }
}

- (void)playSong:(UIButton*) playButton{
    [self configureAVAudioPlayer];
    if(self.isPlaying == true)
    {
        [self.audioPlayer pause];
        [playButton setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        self.isPlaying = false;
    }
    else{
        [self.audioPlayer play];
        [playButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        self.isPlaying = true;
    }
}

- (void)audioPlayerDidFinishPlaying:(AVPlayer *)player successfully:(BOOL)flag{
    
}

- (void)audioPlayerDecodeErrorDidOccur:(AVPlayer *)player error:(NSError *)error{
    
}


+ (ADAudioPlayer*)sharedAudioPlayer{
    static ADAudioPlayer* audioPlayer;
    @synchronized(self) {
        if(audioPlayer == nil){
            audioPlayer = [[ADAudioPlayer alloc] init];
        }
        return audioPlayer;
    }
}

- (id)init {
    if (self = [super init]) {
        self.audioPlayer = [[AVPlayer alloc] init];
        [self.audioPlayer setVolume:1.0];
    }
    return self;
}

- (void) configureAVAudioPlayer {
    //configure audio session
    NSError *setCategoryError = nil;
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    BOOL setCategorySuccess = [[AVAudioSession sharedInstance]
                               setCategory:AVAudioSessionCategoryPlayback
                               withOptions:AVAudioSessionCategoryOptionAllowBluetooth
                               error:&setCategoryError];
    
    if (setCategorySuccess) {
        NSLog(@"Audio Session options set. %@", setCategoryError);
    } else {
        NSLog(@"WARNING: Could not set audio session options. %@",setCategoryError);
    }
}
@end
