//
//  Chapitres.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>
#import "Verset.h"

@interface Chapitre : RLMObject

@property int index;
@property NSString* damId;
@property NSString* bookId;
@property NSString *chapterName;
@property RLMArray<Verset*><Verset>* versets;

+(Chapitre*) getChapitreByIndex:(int)index withDamId:(NSString*)damId andBookId:(NSString*)bookId;

@end
RLM_ARRAY_TYPE(Chapitre)