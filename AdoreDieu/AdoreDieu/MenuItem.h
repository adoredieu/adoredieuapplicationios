//
//  MenuItem.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 22/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property NSString* name;
@property NSString* imgName;
-(id) initWithName:(NSString*)name imgName:(NSString*)imgname;
@end
