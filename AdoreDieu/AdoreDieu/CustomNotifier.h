//
//  CustomNotifier.h
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomNotifierRest.h"
#import "ThoughtAndEnseignementRest.h"

@interface CustomNotifier : NSObject

typedef enum {lastThoughtDate, lastEnseignementDate} DatePreferences;
#define value(enum) [@[@"lastThoughtDate",@"lastEnseignementDate"] objectAtIndex:enum]

- (id) initCustomNotifier;
- (BOOL) checkIfNewThoughtIsAvailable;
- (BOOL) checkIfNewEnseignementIsAvailable;
- (void) displayNotificationsWithTitle:(NSString*) title andBody:(NSString*) body;

@end
