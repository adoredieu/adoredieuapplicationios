//
//  ThoughtAndEnseignementRest.m
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "ThoughtAndEnseignementRest.h"
#import "CustomNotifier.h"

@implementation ThoughtAndEnseignementRest

-(Enseignement*) getLastEnseignement {
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    NSString* url = @"http://www.adoredieu.com/mobility/enseignements.php";
    
    __block Enseignement* enseignement = [[Enseignement alloc] init];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]])
        {
            //Récuperation du premier tableau JSON dans arr.
            NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
            
            for(NSDictionary* dictionnary in arr)
            {
                enseignement.Id = [dictionnary objectForKey:@"id"];
                enseignement.publish_up = [dictionnary objectForKey:@"publish_up"];
                enseignement.article_link = [dictionnary objectForKey:@"article_link"];
                enseignement.title = [dictionnary objectForKey:@"title"];
                enseignement.introtex = [dictionnary objectForKey:@"introtext"];
                //enseignement.thumbnail = [NSURL URLWithString:[[dictionnary objectForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                break;
            }
            [[NSUserDefaults standardUserDefaults] setObject:[[NSDate alloc] init] forKey: value(lastEnseignementDate)];
            dispatch_semaphore_signal(semaphore);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        dispatch_semaphore_signal(semaphore);
        NSLog(@"Error: %@", error);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    return enseignement;
}

-(Pensees*) getLastPensees {
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    NSString* url = @"http://www.adoredieu.com/mobility/pensees.php";
    
    __block Pensees* pensees = [[Pensees alloc] init];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]])
        {
            //Récuperation du premier tableau JSON dans arr.
            NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
            
            for(NSDictionary* dictionnary in arr)
            {
                pensees.Id = [dictionnary objectForKey:@"id"];
                pensees.publish_up = [dictionnary objectForKey:@"publish_up"];
                pensees.article_link = [dictionnary objectForKey:@"article_link"];
                pensees.title = [dictionnary objectForKey:@"title"];
                pensees.introtex = [dictionnary objectForKey:@"introtext"];
                pensees.thumbnail = [NSURL URLWithString:[[dictionnary objectForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]];
                break;
            }
            [[NSUserDefaults standardUserDefaults] setObject:[[NSDate alloc] init] forKey:value(lastThoughtDate)];
            dispatch_semaphore_signal(semaphore);
            NSLog(@"JSON: %@", responseObject);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        dispatch_semaphore_signal(semaphore);
        NSLog(@"Error: %@", error);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    return pensees;
}

@end
