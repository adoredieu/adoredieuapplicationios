//
//  IntroductionViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 16/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "IntroductionViewController.h"
#import "RootViewController.h"

static NSString * const welcome = @"Bienvenue sur l'application AdoreDieu.com";
static NSString * const sampleDescription2 = @"Profitez des pensées riches et intenses & des enseignements.";
static NSString * const sampleDescription3 = @"Accedez à une liste de musiques d'artistes.";
static NSString * const sampleDescription4 = @"Lisez la bible avec ou sans connexion internet.";

@interface IntroductionViewController () <EAIntroDelegate> {
    EAIntroView *_intro;
}

@end

@implementation IntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showIntroWithCrossDissolve];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*!
 * @discussion Introduction of the application
 * @return void
 */
- (void)showIntroWithCrossDissolve {
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"Welcome";
    page1.desc = welcome;
    page1.bgImage = [UIImage imageNamed:@"appadore"];
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_daily_verse"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = @"Pensées & Enseignements";
    page2.desc = sampleDescription2;
    page2.bgImage = [UIImage imageNamed:@"homme-black02blur"];
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_citation"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = @"Lecteur musique";
    page3.desc = sampleDescription3;
    page3.bgImage = [UIImage imageNamed:@"crainrien"];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_audio_player"]];
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = @"Bible";
    page4.desc = sampleDescription4;
    page4.bgImage = [UIImage imageNamed:@"hommbiblealamainblur"];
    page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_bible_big"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.introView.bounds andPages:@[page1,page2,page3, page4]];
    intro.skipButton = nil;
    [intro setDelegate:self];
    
    [intro showInView:self.introView animateDuration:0.0];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"alreadyLaunch"];
}

/*!
 * @discussion called when intro is finished
 * @param EAIntroView*
 * @return void
 */
- (void) introDidFinish:(EAIntroView *)introView {
    //Instanciation du view controller d'accueil
    [self instantiateHomeViewController];
}

- (void) instantiateHomeViewController {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"rootController"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:NULL];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
