//
//  CustomNotifierRest.h
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+AFNetworking.h"
#import "AFHTTPSessionManager.h"

@interface CustomNotifierRest : NSObject

-(BOOL) checkDateRequestWithUrl:(NSString*) url andUserPreference:(NSString*) pref;
@end
