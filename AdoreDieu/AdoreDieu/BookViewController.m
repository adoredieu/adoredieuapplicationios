//
//  BookViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 14/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "BookViewController.h"
#import "DataService.h"
#import <AFNetworking/AFNetworking.h>
#import "BibleDataService.h"

@interface BookViewController ()

@end

@implementation BookViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bookPickerView.backgroundColor = [UIColor clearColor];
    
    //Initialisation du tableau de livres;
    self.books = [[NSMutableArray alloc] init];
    self.chapters = [[NSMutableArray alloc] init];
    self.verses = [[NSMutableArray alloc] init];
    
    DataService* dataService = [DataService sharedDataService];
    if([dataService canConnectToWeb])
    {
        // Do any additional setup after loading the view.
        [self loadBooksWithDamId:self.damId];
        
    }
    else{
        /*RLMResults<Livre>* bibles = [Livre objectsWhere:@"",];
        for(Bible *v in bibles)
        {
            NSMutableArray* volume = [[NSMutableArray alloc] init];
            [volume addObject:@{@"volume_name":v.versionName}];
            [volume addObject:@{@"collection_name":@""}];
            [volume addObject:@{@"damId":v.damId}];
            [self.Bibles addObject:volume];
        }*/
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if( component == 0)
    {
        return [self.books count];
    }
    else if( component == 1)
    {
        return [self.chapters count];
    }
    else
    {
        return [self.verses count];
    }
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger firstComponentRow = [self.bookPickerView selectedRowInComponent:0];
    NSInteger secondComponentRow = [self.bookPickerView selectedRowInComponent:1];
    
    DBTBook* book = [[DBTBook alloc] init];
    book = [self.books objectAtIndex:(NSUInteger)firstComponentRow];

    DBTChapter* chapter = [[DBTChapter alloc] init];
    chapter = [self.chapters objectAtIndex:(NSUInteger)secondComponentRow];
    
    self.bookId = book.bookId;
    

    [self loadChaptersWithDamId:self.damId bookId:self.bookId chapter:@([chapter.chapterId intValue])];

}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Row";
    label.textColor = [UIColor whiteColor];
    return label;
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if( component == 0)
    {
       return [[self.books objectAtIndex:row] bookName];
    }
    else if(component == 1)
    {
        return [[self.chapters objectAtIndex:row] chapterName];
    }
    else{
        return [[NSString alloc] initWithFormat:@"%@",[[self.verses objectAtIndex:row] verseId]] ;
    }
}

-(bool) anySubViewScrolling:(UIView*)view
{
    if( [ view isKindOfClass:[ UIScrollView class ] ] )
    {
        UIScrollView* scroll_view = (UIScrollView*) view;
        if( scroll_view.dragging || scroll_view.decelerating )
        {
            return true;
        }
    }
    
    for( UIView *sub_view in [ view subviews ] )
    {
        if( [ self anySubViewScrolling:sub_view ] )
        {
            return true;
        }
    }
    
    return false;
}

-(void)loadBooksWithDamId:(NSString *)damId
{
    if([self anySubViewScrolling:self.bookPickerView] == false)
    {
        [DBT getLibraryBookWithDamId:self.damId
                             success:^(NSArray *v_books) {
                                 NSLog(@"Books: %@", v_books);
                                 for(DBTBook* book in v_books)
                                 {
                                     [self.books addObject:book];
                                 }
                                 [self loadChaptersWithDamId:damId bookId:[[self.books objectAtIndex:0] bookId] chapter:@1];
                             } failure:^(NSError *error) {
                                 NSLog(@"Error: %@", error);
                             }];
    }
}

-(void)loadChaptersWithDamId:(NSString *)damId bookId:(NSString *)bookId chapter:(NSNumber *)chapterId
{
    if([self anySubViewScrolling:self.bookPickerView] == false)
    {
        [self.chapters removeAllObjects];
        [DBT getLibraryChapterWithDamId:damId
                                 bookId:bookId
                                success:^(NSArray *libraryBookNames) {
                                    //NSLog(@"Library Book Names: %@", libraryBookNames);
                                    for(DBTChapter *chapter in libraryBookNames)
                                    {
                                        [self.chapters addObject:chapter];
                                    }
                                    [self loadVersesDamId:damId book:bookId chapter:chapterId];
                                } failure:^(NSError *error) {
                                    NSLog(@"Error: %@",error);
                                }];
    }
}

-(void)loadVersesDamId:(NSString *)damId book:(NSString *)bookId chapter:(NSNumber *)chapterId
{
    if([self anySubViewScrolling:self.bookPickerView] == false)
    {
        [self.verses removeAllObjects];
        self.allVerses = [[NSMutableString alloc] initWithString:@""];
        [DBT getTextVerseWithDamId:damId
                              book:bookId
                           chapter:chapterId
                        verseStart:nil
                          verseEnd:nil
                           success:^(NSArray *verses) {
                               //NSLog(@"Verses: %@", verses);
                               for(DBTVerse* verse in verses)
                               {
                                   [self.verses addObject:verse];
                               }
                              
                               NSString* n_verse = @"";
                               
                               for(DBTVerse* verse in verses)
                               {
                                   [self.allVerses appendString:[NSString stringWithFormat:@"[%@] ",verse.verseId]];
                                   n_verse = [verse.verseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                   [self.allVerses appendString:n_verse];
                               }
                               [self.content setText:self.allVerses];
                               [self.content reloadInputViews];
                               [self.bookPickerView reloadAllComponents];
                           } failure:^(NSError *error)
                           {
                               NSLog(@"Error: %@", error);
                           }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
