//
//  Citation.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 22/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pensees : NSObject

@property NSString* Id;
@property NSString* publish_up;
@property NSString* article_link;
@property NSString* title;
@property NSString* introtex;
@property NSURL* thumbnail;


@end
