//
//  Citation.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 11/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitationImage : NSObject

@property NSString* Id;
@property NSString* publish_up;
@property NSString* article_link;
@property NSString* title;
@property NSString* introtex;
@property NSURL* thumbnail;
@property NSURL* facebook_link_html;
@property NSURL* fb_article_link;


@end
