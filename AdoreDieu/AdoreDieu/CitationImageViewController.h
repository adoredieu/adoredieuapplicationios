//
//  CitationImageViewController.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 11/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADLivelyCollectionView.h"

@interface CitationImageViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *citationItems;
+ (int) page;
- (void)addPage;
-(void)loadMoreCitationImage:(int)page;
@property CATransform3D animation;
@property int lastContentOffset;
@end
