//
//  ThoughtAndEnseignementRest.h
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "Enseignement.h"
#import "Pensees.h"

@interface ThoughtAndEnseignementRest : NSObject

-(Enseignement*) getLastEnseignement;
-(Pensees*) getLastPensees;

@end
