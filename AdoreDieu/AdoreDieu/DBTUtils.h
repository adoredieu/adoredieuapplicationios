//
//  DBTUtils.h
//  AdoreDieu
//
//  Created by Jeremy on 06/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataService.h"
#import "DBT.h"
#import "Verset.h"
#import "AncienTestament.h"
#import "NouveauTestament.h"
#import "Chapitre.h"
#import "DBTChapter.h"
#import <UIKit/UIKit.h>

@interface DBTUtils : NSObject

+(void)loadVerses:(NSString *)damId book:(NSString *)bookId chapter:(NSNumber *)chapterId  textView:(UITextView*)textView;
+(NSMutableArray<Chapitre*>*)loadChaptersWithDamId:(NSString *)damId bookId:(NSString *)bookId;
@end
