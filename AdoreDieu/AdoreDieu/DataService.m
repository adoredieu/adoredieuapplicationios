//
//  DataService.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 22/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "DataService.h"
#import "Reachability.h"

@implementation DataService

+ (DataService*) sharedDataService {
    static DataService* sharedInstance = nil;
    
    if (sharedInstance == nil) {
        sharedInstance = [[DataService alloc] init];
    }
    
    return sharedInstance;
}

- (BOOL)canConnectToWeb
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return NO;
    } else {
        NSLog(@"There IS internet connection");
        return YES;
    }
}


@end
