//
//  AncienTestament.m
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "AncienTestament.h"

@implementation AncienTestament

+(AncienTestament*) getAncienTestamentByDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<AncienTestament *>* bbs = [AncienTestament objectsWithPredicate:predicate];
    return [bbs objectAtIndex:0];
}

+(NSMutableArray<AncienTestament*>*) getAnciensTestamentsByDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<AncienTestament *>* bbs = [AncienTestament objectsWithPredicate:predicate];
    NSMutableArray<AncienTestament*> *atArray = [[NSMutableArray alloc] init];
    for (AncienTestament* at in bbs) {
        [atArray addObject:at];
    }
    return atArray;
}

+ (BOOL) deleteAncienTestamentWithDamId:(NSString*) damId{
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObject:[self getAncienTestamentByDamId:damId]];
    [realm commitWriteTransaction];
    return true;
}

@end
