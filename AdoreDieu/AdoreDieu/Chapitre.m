//
//  Chapitres.m
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "Chapitre.h"

@implementation Chapitre

+ (Chapitre*) getChapitreByIndex:(int)index withDamId:(NSString*)damId andBookId:(NSString*)bookId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"index = %d AND damId = %@ AND bookId = %@",index,damId,bookId];
    //Recherche d'une bible avec le prédicat
    RLMResults<Chapitre*>* chapitres = [Chapitre objectsWithPredicate:predicate];
    return [chapitres objectAtIndex:0];
}

@end
