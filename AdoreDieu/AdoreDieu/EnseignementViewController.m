//
//  EnseignementViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 11/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "EnseignementViewController.h"
#import "EnseignementDetailViewController.h"
#import "Enseignement.h"
#import "EnseignementCollectionViewCell.h"
#import "DataService.h"
#import "AFNetworking.h"
#import "DEMONavigationController.h"
#import "UIImageView+AFNetworking.h"

@interface EnseignementViewController ()

@end

@implementation EnseignementViewController

//Initialisation de la page à 0.
static int page = 0;

static NSString * const reuseIdentifier = @"EnseignementsItemCell";

+ (int) page
{
    return page;
}

- (void) addPage
{
    page = page +1 ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Enseignements";
    
    page = 0;
    
    //Initialisation de la liste des citations.
    self.EnseignementsItems = [[NSMutableArray alloc] init];
    
    [self.collectionView dataSource];
    [self.collectionView delegate];
    
    [self loadMoreArticle:page];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.EnseignementsItems.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EnseignementCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EnseignementsItemCell" forIndexPath:indexPath];
    
    //Index de la cellule
    NSInteger row = indexPath.row;
    
    //Initialisation de la cellule avec les paramètres de la citation.
    Enseignement* Enseignement = [self.EnseignementsItems objectAtIndex:row];
    cell.titre.text = Enseignement.title;
    cell.introtext.text = [Enseignement.introtex stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""]; ;
    cell.publish_date.text = Enseignement.publish_up;
    
    [cell.imgIndicator startAnimating];
    cell.imgIndicator.hidesWhenStopped = YES;
    
    [cell.thumbnail setImageWithURLRequest: [NSURLRequest requestWithURL:Enseignement.thumbnail] placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [cell.thumbnail setImage:image];
        [cell.imgIndicator stopAnimating];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        cell.imgIndicator.hidesWhenStopped = YES;
        [cell.imgIndicator stopAnimating];
    }];
    
    return cell;
}

/*!
 * @discussion set cell fullScreenSize
 * @param (UICollectionView *)collectionView
 * @param (UICollectionViewLayout *)collectionViewLayout
 * @param (NSIndexPath *)indexPath
 * @return CGSize
 */
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGFloat width = self.view.bounds.size.width/2.4;
        return CGSizeMake(width, width*1.3);
    }
    else {
        CGFloat width = self.view.bounds.size.width;
        return CGSizeMake(width, width*1.3);
    }
}

NSInteger const SMEPGiPadViewControllerCellWidth = 332;

/*!
 * @discussion center cells for the collectionView
 * @param UICollectionView
 * @param UICollectionViewLayout
 * @param NSInteger
 * @return UIEdgetInsets
 */
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSInteger numberOfCells = self.view.frame.size.width / SMEPGiPadViewControllerCellWidth;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * SMEPGiPadViewControllerCellWidth)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    //return UIEdgeInsetsMake(0, 0, 0, 0);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

}

/*!
 * @discussion Displaying cells
 * @param UICollectionViewCell, NSIndexPath
 * @return void
 */
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0.6;
    
    cell.layer.transform = self.animation;
    cell.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)
    {
        //Charger plus d'articles quand on atteint le bas de la page.
        [self loadMoreArticle:page];
    }
    
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(1.1, 1.1, 0);
        self.animation = CATransform3DTranslate(scale, 0, -70, 0);
        NSLog(@"Scrolling Up");
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(0.9, 0.9, 0);
        self.animation = CATransform3DTranslate(scale, 0, 70, 0);
        NSLog(@"Scrolling Down");
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

-(void)loadMoreArticle:(int)page
{
    DataService* dataService = [DataService sharedDataService];
    
    if([dataService canConnectToWeb])
    {
        
        NSString* url = @"http://www.adoredieu.com/mobility/enseignements.php?page=";
        url = [url stringByAppendingString:[NSString stringWithFormat:@"%d", page]];
        [self addPage];
        /*
         Requete JSON, et parsing de la réponse json reçue.
         */
        
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:url
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if([responseObject isKindOfClass:[NSArray class]])
                 {
                     //Récuperation du premier tableau JSON dans arr.
                     NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
                     
                     for(NSDictionary* dictionnary in arr)
                     {
                         Enseignement* enseignement = [[Enseignement alloc] init];
                         enseignement.Id = [dictionnary objectForKey:@"id"];
                         enseignement.publish_up = [dictionnary objectForKey:@"publish_up"];
                         enseignement.article_link = [dictionnary objectForKey:@"article_link"];
                         enseignement.title = [dictionnary objectForKey:@"title"];
                         enseignement.introtex = [dictionnary objectForKey:@"introtext"];
                         enseignement.thumbnail = [NSURL URLWithString:[[dictionnary objectForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                         [self.EnseignementsItems addObject:enseignement];
                     }
                     
                     NSLog(@"JSON: %@", responseObject);
                 }
                 [self.collectionView reloadData];
             }
             failure:^(NSURLSessionDataTask *task, id responseObject) {
                 NSLog(@"JSON: %@", responseObject);
             }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Enseignement detail segue"])
    {
        NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        // Get reference to the destination view controller
        EnseignementDetailViewController *vc = [segue destinationViewController];
        
        //Index de la cellule
        NSInteger row = selectedIndexPath.row;
        
        //Initialisation de la cellule avec les paramètres de la citation.
        Enseignement* p = [self.EnseignementsItems objectAtIndex:row];
        
        // Pass any objects to the view controller here, like...
        vc.Id = p.Id;
        vc.thumbnail = p.thumbnail;
    }
}

- (IBAction)showMenu:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


@end
