//
//  CustomNotifier.m
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "CustomNotifier.h"

@implementation CustomNotifier


/*!
 * @discussion init new custom notifier
 * @return CustomNotifier*
 */
- (id) initCustomNotifier {
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    NSTimer* timerNotifier = [NSTimer scheduledTimerWithTimeInterval:10
                                     target:self
                                   selector:@selector(timerNotifierWithTimer:)
                                   userInfo:nil
                                    repeats:true];
    [[NSRunLoop currentRunLoop] addTimer:timerNotifier forMode:NSRunLoopCommonModes];
    return self;
}

/*!
 * @discussion timer wich call both check functions
 * @param NSTimer*
 * @return void
 */
- (void) timerNotifierWithTimer:(NSTimer*) timer {
    ThoughtAndEnseignementRest* rest = [[ThoughtAndEnseignementRest alloc] init];
    if([self checkIfNewEnseignementIsAvailable] == YES) {
        Enseignement* enseignement = [rest getLastEnseignement];
        [self displayNotificationsWithTitle:enseignement.title andBody:enseignement.introtex];
    }
    if([self checkIfNewThoughtIsAvailable] == YES) {
        Pensees* pensees = [rest getLastPensees];
        [self displayNotificationsWithTitle:pensees.title andBody:pensees.introtex];
    }
}

/*!
 * @discussion check if a new thought is available
 * @return BOOL
 */
- (BOOL) checkIfNewThoughtIsAvailable {
    CustomNotifierRest* restNotifier = [[CustomNotifierRest alloc] init];
    NSString* url = @"http://www.adoredieu.com/mobility/pensees.php?getLastDate=1";
    return [restNotifier checkDateRequestWithUrl:url andUserPreference:[[NSUserDefaults standardUserDefaults] objectForKey: value(lastThoughtDate)]];
}

/*!
 * @discussion Check if a new thought is available
 * @return BOOL
 */
- (BOOL) checkIfNewEnseignementIsAvailable {
    CustomNotifierRest* restNotifier = [[CustomNotifierRest alloc] init];
    NSString* url = @"http://www.adoredieu.com/mobility/enseignements.php?getLastDate=1";
    return [restNotifier checkDateRequestWithUrl:url andUserPreference:[[NSUserDefaults standardUserDefaults] objectForKey: value(lastEnseignementDate)]];
}

/*!
 * @discussion display notifications when a new thought or enseignement is available
 * @return void
 */
- (void) displayNotificationsWithTitle:(NSString*) title andBody:(NSString*) body {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1000];
    notification.alertBody = body;
    notification.alertTitle = title;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}
@end
