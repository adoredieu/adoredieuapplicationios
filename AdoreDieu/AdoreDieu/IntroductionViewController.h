//
//  IntroductionViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 16/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"

@interface IntroductionViewController : UIViewController

@property (weak, nonatomic) IBOutlet EAIntroView *introView;

@end
