//
//  MusiqueViewTableViewCell.h
//  AdoreDieu
//
//  Created by Jeremy on 26/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusiqueViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageAlbum;
@property (weak, nonatomic) IBOutlet UILabel *Title;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;

@end
