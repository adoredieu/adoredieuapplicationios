//
//  PenseeDetailViewController.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 12/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADLivelyCollectionView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <AFNetworking/AFNetworking.h>

@interface PenseeDetailViewController : UIViewController
@property NSString* Id;
@property NSURL* thumbnail;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *publish_date;
@property (strong, nonatomic) IBOutlet FBSDKLikeButton *fb_article_link;
@property (weak, nonatomic) IBOutlet UILabel *titre;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;
@property CGFloat oldHeight;
@property CGFloat lastContentOffSet;
@property (weak, nonatomic) IBOutlet UILabel *content;

@end
