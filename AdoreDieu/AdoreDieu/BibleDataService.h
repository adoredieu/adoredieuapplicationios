//
//  BibleDataServce.h
//  AdoreDieu
//
//  Created by Jeremy on 12/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KVNProgress/KVNProgress.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <AFNetworking/AFNetworking.h>
#import "DataService.h"
#import "AFNetworking.h"
#import "Bible.h" 
#import <Realm/Realm.h>

@interface BibleDataService : NSObject

//@property RLMRealm* realm;
@property int verseNumber;
@property int vMaxNumber;
@property dispatch_queue_t queueToRunBDS;

- (void) downloadBibleWithDamId:(NSString*) damId;
- (void) downloadBookWithNameOfBook:(NSString*) bookName;
- (BOOL)checkIfAtOrNtIsAvailableWithDamId:(NSString*) damId;
- (void) downloadChapter;
- (void) downloadVerse;
- (void) updateLoaderStatus;
@property RLMResults<Bible>* bibles;

@end
