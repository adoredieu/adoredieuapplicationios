//
//  AudioPlayerViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 26/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusiqueViewTableViewCell.h"
#import "AFNetworking/AFNetworking.h"
#import "DataService.h"
#import "AFNetworking.h"
#import <AVFoundation/AVFoundation.h>
#import "ADAudioPlayer.h"
#import "REFrostedViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface AudioPlayerViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate>
@property MPRemoteCommandCenter* commandCenter;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray* musiques;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) ADAudioPlayer *audioPlayer;
@property BOOL isPlaying;
@property (weak, nonatomic) IBOutlet UIImageView* albumCover;
@property (weak, nonatomic) IBOutlet UISlider* currentTimeAudioSlider;
@property (weak, nonatomic) IBOutlet UILabel* DurationTime;
@property (weak, nonatomic) IBOutlet UILabel* songName;
@property (weak, nonatomic) IBOutlet UILabel* artiste;
@property (weak, nonatomic) IBOutlet UILabel* currentTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton* playButton;
@property NSInteger selectedRow;
- (void)audioPlayerDidFinishPlaying:(AVPlayer *)player successfully:(BOOL)flag;
- (void)audioPlayerDecodeErrorDidOccur:(AVPlayer *)player error:(NSError *)error;
- (void) pauseCommandCenter;
- (void) playCommandCenter;

@end
