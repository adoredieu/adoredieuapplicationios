//
//  MenuItemCollectionViewCell.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 20/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MenuImage;
@property (weak, nonatomic) IBOutlet UILabel *MenuLabel;
@property (strong, nonatomic) IBOutlet UIVisualEffectView *blurEffect;

@end
