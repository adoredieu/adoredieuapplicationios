//
//  AppDelegate.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 13/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import <AudioToolBox/AudioToolBox.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, REFrostedViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

