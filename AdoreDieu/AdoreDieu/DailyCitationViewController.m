//
//  ViewController.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 13/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "DailyCitationViewController.h"
#import "AFHTTPSessionManager.h"

@interface DailyCitationViewController ()

@end

@implementation DailyCitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    // Do any additional setup after loading the view.
    NSString* url = @"http://www.adoredieu.com/mobility/citation.php";
    
    /*
     Requete JSON, et parsing de la réponse json reçue.
     */
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSString* response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
             [self.citationUI setText:response];
         }
         failure:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
         }];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
