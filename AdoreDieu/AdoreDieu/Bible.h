//
//  Bible.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>
#import "AncienTestament.h"
#import "NouveauTestament.h"

@interface Bible : RLMObject

@property AncienTestament* at;
@property NouveauTestament* nt;
@property NSString* versionCode;
@property NSString* versionName;
@property NSString* damId;
@property BOOL downloaded;

+ (Bible*)getBibleByDamId:(NSString*)damId;

+ (void) deleteAtOrNtByDamId:(NSString*)damId andCollectionCode:(NSString*)collectioncode;
@end
RLM_ARRAY_TYPE(Bible)