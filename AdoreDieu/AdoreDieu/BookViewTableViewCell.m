//
//  BookViewTableViewCell.m
//  AdoreDieu
//
//  Created by Jeremy on 19/02/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "BookViewTableViewCell.h"

@implementation BookViewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
