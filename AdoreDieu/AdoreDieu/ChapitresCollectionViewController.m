//
//  ChapitresCollectionViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 05/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "ChapitresCollectionViewController.h"
#import "DBTUtils.h"

@interface ChapitresCollectionViewController ()

@end

@implementation ChapitresCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.verset = [[NSMutableString alloc] init];
    self.navigationItem.title = self.livre.bookName;
    self.chapitres = [DBTUtils loadChaptersWithDamId:self.livre.damId bookId:self.livre.bookId];
    if(self.chapitres.count > 0)
    {
        Chapitre* chapitre = [self.chapitres objectAtIndex:0];
        [DBTUtils loadVerses:chapitre.damId book:chapitre.bookId chapter:[NSNumber numberWithInt:chapitre.index] textView:self.textView];
        /*for (Verset* v in self.versets) {
            [self.verset appendString:v.verset];
        }*/
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.chapitres.count;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    Chapitre* chapitre = [self.chapitres objectAtIndex:row];
    [DBTUtils loadVerses:chapitre.damId book:chapitre.bookId chapter:[NSNumber numberWithInt:chapitre.index] textView:self.textView];
    /*for (Verset* v in self.versets) {
        [self.verset appendString:v.verset];
    }
    self.textView.text = self.verset;*/
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [NSString stringWithFormat:@"chapitre %d",[[self.chapitres objectAtIndex:row] index]];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat:@"chapitre %d",[[self.chapitres objectAtIndex:row] index]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
