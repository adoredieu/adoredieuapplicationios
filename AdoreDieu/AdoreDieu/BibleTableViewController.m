//
//  BibleTableViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 13/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "BibleTableViewController.h"
#import "DBT.h"
#import "BibleTableViewCell.h"
#import "DataService.h"
#import "BooksViewController.h"
#import "BibleDataService.h"
#import "Bible.h"
#import "pthread.h"
#import "AncienTestament.h"
#import "NouveauTestament.h"
#import "REFrostedViewController.h"

@interface BibleTableViewController ()

@end

@implementation BibleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Bibles"];
    self.Bibles = [[NSMutableArray alloc] init];
    DataService* dataService = [DataService sharedDataService];
    if([dataService canConnectToWeb])
    {
        [DBT getLibraryVolumeWithDamID:nil
                          languageCode:@"FRN"
                                 media:@"text"
                               success:^(NSArray *volumes) {
                                   NSLog(@"Volumes %@", volumes);
                                   
                                   if([volumes isKindOfClass:[NSArray class]])
                                   {
                                       //Récuperation du premier tableau JSON dans arr.
                                       NSMutableArray *arr = [NSMutableArray arrayWithArray:volumes];
                                       
                                       for(DBTVolume* v in arr)
                                       {
                                           NSMutableArray* volume = [[NSMutableArray alloc] init];
                                           [volume addObject:@{@"volume_name":v.versionName}];
                                           [volume addObject:@{@"collection_name":v.collectionName}];
                                           [volume addObject:@{@"damId":v.damId}];
                                           [volume addObject:@{@"version_code":v.collectionCode}];
                                           [self.Bibles addObject:volume];
                                       }
                                   }
                                   [self.tableView reloadData];
                               } failure:^(NSError *error) {
                                   NSLog(@"Error: %@", error);
                               }];
    }
    else{
        RLMResults<Bible>* bibles = [Bible allObjects];
        for(Bible *v in bibles)
        {
            if(v.at != nil)
            {
                NSMutableArray* volume = [[NSMutableArray alloc] init];
                [volume addObject:@{@"volume_name":v.versionName}];
                [volume addObject:@{@"collection_name":@"Ancien Testament"}];
                [volume addObject:@{@"damId":v.at.damId}];
                [self.Bibles addObject:volume];
            }
            if(v.nt != nil)
            {
                NSMutableArray* volume = [[NSMutableArray alloc] init];
                [volume addObject:@{@"volume_name":v.versionName}];
                [volume addObject:@{@"collection_name":@"Nouveau Testament"}];
                [volume addObject:@{@"damId":v.nt.damId}];
                [self.Bibles addObject:volume];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.Bibles count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BibleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"biblecell" forIndexPath:indexPath];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    cell.selectedBackgroundView = blurEffectView;
    cell.backgroundColor = [UIColor clearColor];
    
    NSInteger row = indexPath.row;
    NSMutableArray* v_array = [self.Bibles objectAtIndex:row];
    NSDictionary* v_name_dict = v_array[0];
    NSDictionary* v_collecname_dict = v_array[1];
    
    cell.titre.text = [v_name_dict objectForKey:@"volume_name"];
    cell.titre.frame = CGRectMake(cell.titre.frame.origin.x, cell.titre.frame.origin.y, 150, cell.titre.frame.size.height);
    cell.detail.text = [v_collecname_dict objectForKey:@"collection_name"];
    
    //Ajout du switch sur les lignes
    UISwitch* s = [[UISwitch alloc] init];
    CGSize switchSize = [s sizeThatFits:CGSizeZero];
    s.frame = CGRectMake(cell.contentView.bounds.size.width - switchSize.width - 5.0f,
                         (cell.contentView.bounds.size.height - switchSize.height) / 2.0f,
                         switchSize.width,
                         switchSize.height);
    
    BibleDataService* bds = [BibleDataService alloc];
    if([bds checkIfAtOrNtIsAvailableWithDamId:[v_array[2] objectForKey:@"damId"]])
    {
        s.on = true;
    }
    
    s.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    s.tag = 100;
    s.backgroundColor = [UIColor clearColor];
    [s addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:s];
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Book segue"])
    {
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        // Get reference to the destination view controller
        BooksViewController *vc = [segue destinationViewController];
        
        //Index de la row
        NSInteger row = selectedIndexPath.row;
        
        //Initialisation de la cellule avec le damId pour télécharger le livre.
        NSMutableArray* v_array = [self.Bibles objectAtIndex:row];
        NSDictionary* v_damId = v_array[2];
        
        if([[NouveauTestament getNouveauxTestamentsByDamId:[v_damId objectForKey:@"damId"]] count] > 0)
        {
            vc.haveToDownload = NO;
            vc.livres = [[NouveauTestament getNouveauTestamentByDamId:[v_damId objectForKey:@"damId"]] livres];
        }
        else if([[AncienTestament getAnciensTestamentsByDamId:[v_damId objectForKey:@"damId"]] count] > 0)
        {
            vc.haveToDownload = NO;
            vc.livres = [[AncienTestament getAncienTestamentByDamId:[v_damId objectForKey:@"damId"]] livres];
        }
        else{
            vc.haveToDownload = YES;
            vc.damId = [v_damId objectForKey:@"damId"];
        }
    }
}

-(void)dlBibleWithDamId:(id)damId{
    BibleDataService* bds = [BibleDataService alloc];
    [KVNProgress showProgress:0.0 status:@"Téléchargement de la bible en cours"];
    [bds downloadBibleWithDamId:damId];
}

-(void) switchChanged:(id)sender {
    UISwitch* switcher = (UISwitch*)sender;
    // Récupère à quelle row appartient le bouton
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSInteger row = indexPath.row;
    NSMutableArray* v_array = [self.Bibles objectAtIndex:row];
    NSDictionary* v_damId = v_array[2];
    NSDictionary* v_versionCode = v_array[3];
    
    if([switcher isOn])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voulez vous télécharger la bible ?"
                                                        message:@"En téléchargeant la bible vous pourrez accéder à celle-ci hors-connexion."
                                                       delegate:self
                                              cancelButtonTitle:@"Annuler"
                                              otherButtonTitles:@"Confirmer",nil];
        [alert show];
        
        
        //[self.imgIndicator setProgressWithDownloadProgressOfTask:task animated:YES];
        simpleBlock = ^{
           dispatch_queue_t queueToRunBDS = dispatch_queue_create("bibleDataService", NULL);
            [KVNProgress showProgress:0.0 status:@"Téléchargement de la bible en cours"];
            //dispatch_async(queueToRunBDS, ^{
                BibleDataService* bds = [BibleDataService alloc];
                bds.queueToRunBDS = queueToRunBDS;
                [bds downloadBibleWithDamId:[v_damId objectForKey:@"damId"]];
            NSLog(@"Realm Path = %@",[[RLMRealm defaultRealm] path]);
            //});
        };
    }
    else
    {
        [KVNProgress showWithStatus:@"Suppression ..."];
        [Bible deleteAtOrNtByDamId:[v_damId objectForKey:@"damId"] andCollectionCode:[v_versionCode objectForKey:@"version_code"]];
        [KVNProgress showSuccessWithStatus:@"Bible supprimée" completion:dismissDialog];
        dismissDialog = ^{
            [KVNProgress dismiss];
        };
        
    }
    // Store the value and/or respond appropriately
}



void (^dismissDialog)(void) = ^{
    [KVNProgress dismiss];
};

void (^simpleBlock)(void);

- (void) callContinuation
{
    simpleBlock();
}

//Alert view pour agir en conséquence de l'utilisateur
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
    }
    else if (buttonIndex == 1) {
        [self callContinuation];
    }
}

- (IBAction)showMenu:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
