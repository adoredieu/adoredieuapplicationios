//
//  BookViewTableViewCell.h
//  AdoreDieuBibleTableViewCell
//
//  Created by Jeremy on 19/02/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
