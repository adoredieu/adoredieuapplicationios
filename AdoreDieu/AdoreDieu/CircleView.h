//
//  CircleView.h
//  AdoreDieu
//
//  Created by Jeremy on 11/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleView : UIView

@property CAShapeLayer* circlePathLayer;
@property CGFloat circleRadius;

- (instancetype)initWithCoder:(NSCoder *)aDecoder;
- (instancetype)initWithFrame:(CGRect)frame;
- (void)configure;
- (CGRect) circleFrame;
- (UIBezierPath*) circlePath;
- (void)layoutSubviews;
@end
