//
//  AncienTestament.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>
#import "Livre.h"

@interface AncienTestament : RLMObject

@property NSString* damId;
@property int verseNumber;
@property RLMArray<Livre*><Livre>* livres;

+(AncienTestament*) getAncienTestamentByDamId:(NSString *)damId;
+ (BOOL) deleteAncienTestamentWithDamId:(NSString*) damId;
+(NSMutableArray<AncienTestament*>*) getAnciensTestamentsByDamId:(NSString *)damId;
@end
