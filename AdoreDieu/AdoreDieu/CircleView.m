//
//  CircleView.m
//  AdoreDieu
//
//  Created by Jeremy on 11/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.circlePathLayer = [[CAShapeLayer alloc] init];
        self.circleRadius = 20;
        [self configure];
    }
    return self;
}

- (void)configure {
    self.circlePathLayer.frame = self.bounds;
    self.circlePathLayer.lineWidth = 2;
    self.circlePathLayer.fillColor = [[UIColor clearColor] CGColor];
    self.circlePathLayer.strokeColor = [[UIColor redColor] CGColor];
    [self.layer addSublayer:self.circlePathLayer];
    self.backgroundColor = [UIColor whiteColor];
}

- (CGRect) circleFrame{
    CGRect circleFrame = CGRectMake(0, 0, 2*self.circleRadius, 2*self.circleRadius);
    circleFrame.origin.x = CGRectGetMidX(self.circlePathLayer.bounds) - CGRectGetMidX(circleFrame);
    circleFrame.origin.y = CGRectGetMidY(self.circlePathLayer.bounds) - CGRectGetMidY(circleFrame);
    return circleFrame;
}

- (UIBezierPath*) circlePath{
    return [UIBezierPath bezierPathWithOvalInRect:self.circleFrame];
}

- (void)layoutSubviews {
    [self layoutSubviews];
    self.circlePathLayer.frame = self.bounds;
    self.circlePathLayer.path = [[self circlePath] CGPath];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
