//
//  BibleTableViewCell.h
//  AdoreDieu
//
//  Created by Jeremy on 13/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BibleDataService.h"

@interface BibleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titre;
@property (weak, nonatomic) IBOutlet UILabel *detail;

@end
