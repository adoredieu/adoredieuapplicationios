//
//  Livres.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>
#import "Chapitre.h"

@interface Livre : RLMObject

@property NSString* bookId;
@property NSString* bookName;
@property int numberOfChapters;
@property NSString* damId;
@property RLMArray<Chapitre*><Chapitre>* chapitres;

+ (Livre*) getLivreByBookId:(NSString*) bookId andDamId:(NSString*)damId;

@end
RLM_ARRAY_TYPE(Livre)
