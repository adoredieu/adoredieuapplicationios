//
//  CitationCollectionView.h
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 21/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADLivelyCollectionView.h"
#import "DEMONavigationController.h"

@interface PenseesCollectionView : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *PenseesItems;
@property CATransform3D animation;
@property int lastContentOffset;
+ (int) page;
- (void)addPage;
-(void)loadMoreArticle:(int)page;

@end
