//
//  ChapitresCollectionViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 05/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chapitre.h"
#import "Livre.h"

@interface ChapitresCollectionViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property NSMutableArray<Verset*>*versets;
@property NSMutableArray<Chapitre*>*chapitres;
@property Livre* livre;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property NSMutableString* verset;

@end
