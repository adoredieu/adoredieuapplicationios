//
//  Livres.m
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "Livre.h"

@implementation Livre

// Retourne un livre par son Id
+(Livre*) getLivreByBookId:(NSString *)bookId andDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"bookId = %@ AND damId= %@",bookId, damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<Livre*>* livres = [Livre objectsWithPredicate:predicate];
    return [livres objectAtIndex:0];
}

@end
