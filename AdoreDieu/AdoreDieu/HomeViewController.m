//
//  DEMOHomeViewController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "HomeViewController.h"
#import "DEMONavigationController.h"
#import "CircleView.h"
#import <QuartzCore/QuartzCore.h>
@interface HomeViewController ()

@end

@implementation HomeViewController


- (void) viewDidLoad {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:15];
    notification.alertBody = @"test";
    notification.alertTitle = @"test2";
    notification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];

    self.title = @"Accueil";
    
    // Do any additional setup after loading the view.
    NSString* url = @"http://www.adoredieu.com/mobility/daily_verse.php";
    
    /*
     Requete JSON, et parsing de la réponse json reçue pour le verset journalier.
     */
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSString* response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
             [self.verseLabel setText:response];
         }
         failure:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
         }];
    
    //Récupération de la citation journalière
    NSString* urlDailyVerse = @"http://www.adoredieu.com/mobility/citation.php";
    
    [manager GET:urlDailyVerse
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSString* response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
             [self.citationLabel setText:response];
         }
         failure:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
         }];
    
    [super viewDidLoad];
}

- (IBAction)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

@end
