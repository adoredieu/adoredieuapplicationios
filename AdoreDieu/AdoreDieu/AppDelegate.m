//
//  AppDelegate.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 13/04/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "DBT.h" 
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "CustomNotifier.h"
#import "ADAudioPlayer.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    /*
     * On vérifie si c'est le premier lancement de l'appli auquel cas on lance 
     * l'intro de l'application.
     */
    BOOL alreadyLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:@"alreadyLaunch"];
    if( alreadyLaunch == NO ){
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"introController"];
        self.window.rootViewController = viewController;
    }
    else {
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"rootController"];
        self.window.rootViewController = viewController;
    }

    ADAudioPlayer* audioPlayer = [ADAudioPlayer sharedAudioPlayer];
    [audioPlayer configureAVAudioPlayer];
    
    [self.window makeKeyAndVisible];
    
    //Define API Key for digital bible school
    [DBT setAPIKey:@"4b0487c377358a13b929584b15c12000"];
    
    // Override point for customization after application launch.

    [FBSDKLikeControl class];
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[CustomNotifier alloc] initCustomNotifier];

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
