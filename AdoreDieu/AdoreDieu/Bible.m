//
//  Bible.m
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "Bible.h"

@implementation Bible

+(Bible*) getBibleByDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<Bible*>* bbs = [Bible objectsWithPredicate:predicate];
    return [bbs objectAtIndex:0];
}

+ (void) deleteAtOrNtByDamId:(NSString*)damId andCollectionCode:(NSString*)collectioncode{
    if([collectioncode  isEqual: @"OT"]){
        //Ancien testament
        [AncienTestament deleteAncienTestamentWithDamId:damId];
    }
    else{
        //Nouveau Testament
        [NouveauTestament deleteNouveauTestamentWithDamId:damId];
    }
}
@end
