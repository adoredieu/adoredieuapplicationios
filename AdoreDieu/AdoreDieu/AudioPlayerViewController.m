//
//  AudioPlayerViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 26/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "AudioPlayerViewController.h"
#import <KVNProgress/KVNProgress.h>
#import "UIImageView+AFNetworking.h"

@interface AudioPlayerViewController ()

@end



@implementation AudioPlayerViewController

@synthesize audioPlayer;

- (void) pauseCommandCenter {
    [self playSong:self.playButton];
}
- (void) playCommandCenter {
    [self playSong:self.playButton];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Configuration du commandCenter
    self.commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    self.commandCenter.playCommand.enabled = true;
    [self.commandCenter.playCommand addTarget:self action:@selector(playCommandCenter)];
    
    self.commandCenter.pauseCommand.enabled = true;
    [self.commandCenter.pauseCommand addTarget:self action:@selector(pauseCommandCenter)];
    
    self.commandCenter.nextTrackCommand.enabled = true;
    [self.commandCenter.nextTrackCommand addTarget:self action:@selector(nextSong:)];
    
    self.commandCenter.previousTrackCommand.enabled = true;
    [self.commandCenter.previousTrackCommand addTarget:self action:@selector(previousSong:)];

    
    self.audioPlayer = [ADAudioPlayer sharedAudioPlayer];
    
    // Do any additional setup after loading the view.
    self.musiques = [[NSMutableArray alloc] init];
    DataService* dataService = [DataService sharedDataService];
    if([dataService canConnectToWeb])
    {
        
        NSString* url = @"http://www.adoredieu.com/mobility/audio_player.php";
        
        /*
         Requete JSON, et parsing de la réponse json reçue.
         */
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:url
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                [KVNProgress showWithStatus:@"Chargement des musiques"];
                 NSLog(@"JSON: %@", responseObject);
                 if([responseObject isKindOfClass:[NSArray class]])
                 {
                     //Récuperation du premier tableau JSON dans arr.
                     NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
                     
                     for(NSDictionary* v in arr)
                     {
                         //NSString* content = [responseObject objectForKey:@"content"];
                         NSMutableArray* zik = [[NSMutableArray alloc] init];
                         NSString* artiste = [v objectForKey:@"artist"];
                         NSString* title = [v objectForKey:@"title"];
                         NSString* cover = [v objectForKey:@"cover"];
                         NSString* file = [v objectForKey:@"file"];
                         [zik addObject:@{@"artiste":artiste}];
                         [zik addObject:@{@"title":title}];
                         [zik addObject:@{@"cover":cover}];
                         [zik addObject:@{@"file":file}];
                         [self.musiques addObject:zik];
                     }
                 }
                 [self.tableView reloadData];
                 if(self.audioPlayer.selectedRow != nil){
                     self.selectedRow = self.audioPlayer.selectedRow.row;
                     NSMutableArray* zik = [self.musiques objectAtIndex:self.audioPlayer.selectedRow.row];
                     NSDictionary* zik_artiste = zik[0];
                     NSDictionary* zik_title = zik[1];
                     NSDictionary* zik_cover = zik[2];
                     
                     self.artiste.text = [zik_artiste objectForKey:@"artiste"];
                     self.songName.text = [zik_title objectForKey:@"title"];
                     //Image de l'album séléctionné
                     NSString* imgUrl = [zik_cover objectForKey:@"cover"];
                     NSURL *imageURL = [NSURL URLWithString:imgUrl];
                     NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                     [self.albumCover setImage:[UIImage imageWithData:imageData]];
                     
                     [self.activityIndicator stopAnimating];
                     
                     if (self.audioPlayer.isPlaying == true) {
                         [self.playButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
                         
                         //Création d'un timer d'une seconde
                         CMTime interval = CMTimeMakeWithSeconds(1.0, NSEC_PER_SEC); // 1 second
                         
                         //Ajouter un observeur périodique qui met à jours les label dans l'UI toutes les secondes
                         id t = [self.audioPlayer.audioPlayer addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
                             
                             //Récupération du temps total de la video
                             int aDuration = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.asset.duration);
                             
                             int aCurrentTime = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.currentTime);
                             
                             if(aCurrentTime < aDuration){
                                 
                                 //Récupération du temps de courant de la musique en cours
                                 int aCurrentTime = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.currentTime);
                                 
                                 int Cminutes = aCurrentTime/60;
                                 int Csecs = aCurrentTime%60;
                                 int Dminutes = aDuration/60;
                                 int Dsecs = aDuration%60;
                                 
                                 NSString* durationTime = [NSString stringWithFormat:@"%.0f:%.0f",round(Dminutes), round(Dsecs)];
                                 
                                 NSString* currentTime = [NSString stringWithFormat:@"%.0f:%.0f", round(Cminutes), round(Csecs)];
                                 
                                 //Mise à jours des labels dans l'UI
                                 [self.currentTimeAudioSlider setMaximumValue:aDuration];
                                 [self.currentTimeAudioSlider setValue:aCurrentTime];
                                 self.DurationTime.text = durationTime;
                                 self.currentTimeLabel.text = currentTime;
                             }
                             else{
                                 //Quand la musique est terminée, on passe à la suivante
                                 [self.audioPlayer.audioPlayer removeTimeObserver:t];
                                 NSIndexPath* path = [NSIndexPath indexPathForRow:self.audioPlayer.selectedRow.row+1 inSection:0];
                                 [self tableView:self.tableView didSelectRowAtIndexPath:path];
                             }
                         }];
                     }
                 }
                 else {
                     [self.activityIndicator stopAnimating];
                     self.albumCover.image = [UIImage imageNamed:@"music_icon.png"];
                 }
                 [KVNProgress dismiss];
             }
             failure:^(NSURLSessionDataTask *task, id responseObject) {
                 [KVNProgress dismiss];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pas de connexion"
                                                                 message:@"Veuillez vous connecter à internet."
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                       otherButtonTitles:@"Confirmer",nil];
                 [alert show];
                 
             }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.musiques count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    MusiqueViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"musiquecell"];

    if (self.audioPlayer.selectedRow != nil) {
        if(self.audioPlayer.selectedRow == indexPath) {
            cell.selected = true;
        }
    }
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    cell.selectedBackgroundView = blurEffectView;
    cell.backgroundColor = [UIColor clearColor];
    
    cell.backgroundColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.5];
    NSInteger row = indexPath.row;
    NSMutableArray* zik = [self.musiques objectAtIndex:row];
    NSDictionary* zik_artiste = zik[0];
    NSDictionary* zik_title = zik[1];
    NSDictionary* zik_cover = zik[2];
    
    cell.Title.text = [zik_title objectForKey:@"title"];
    cell.subtitle.text = [zik_artiste objectForKey:@"artiste"];
    NSString* imgUrl = [zik_cover objectForKey:@"cover"];
    
    [cell.imageAlbum setImageWithURL:[[NSURL alloc] initWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"music_icon.png"]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    self.audioPlayer.selectedRow = indexPath;
    //On sauvegarde la row selectionnée
    self.selectedRow = row;
    
    self.audioPlayer = [ADAudioPlayer sharedAudioPlayer];

    [self.audioPlayer setSong:self.selectedRow musiques:self.musiques songNameLabel:self.songName artisteLabel:self.artiste albumCoverImageView:self.albumCover];
    
    [self.audioPlayer playSong:self.playButton];
    
    [self.activityIndicator stopAnimating];
    
   //Création d'un timer d'une seconde
    CMTime interval = CMTimeMakeWithSeconds(1.0, NSEC_PER_SEC); // 1 second
    
    //Ajouter un observeur périodique qui met à jours les label dans l'UI toutes les secondes
    id t = [self.audioPlayer.audioPlayer addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
        
        //Récupération du temps total de la video
        int aDuration = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.asset.duration);
        
        int aCurrentTime = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.currentTime);
        
        if(aCurrentTime < aDuration){
            
            //Récupération du temps de courant de la musique en cours
            int aCurrentTime = CMTimeGetSeconds(audioPlayer.audioPlayer.currentItem.currentTime);
            
            int Cminutes = aCurrentTime/60;
            int Csecs = aCurrentTime%60;
            int Dminutes = aDuration/60;
            int Dsecs = aDuration%60;
            
            NSString* durationTime = [NSString stringWithFormat:@"%.0f:%.0f",round(Dminutes), round(Dsecs)];
            
            NSString* currentTime = [NSString stringWithFormat:@"%.0f:%.0f", round(Cminutes), round(Csecs)];
            
            //Mise à jours des labels dans l'UI
            [self.currentTimeAudioSlider setMaximumValue:aDuration];
            [self.currentTimeAudioSlider setValue:aCurrentTime];
            self.DurationTime.text = durationTime;
            self.currentTimeLabel.text = currentTime;
            
        }
        else{
            //Quand la musique est terminée, on passe à la suivante
            [self.audioPlayer.audioPlayer removeTimeObserver:t];
            self.audioPlayer.isPlaying = false;
            NSIndexPath* path = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
            [self tableView:tableView didSelectRowAtIndexPath:path];
        }
    }];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    NSLog(@"Audio finished playing succesfully");
}
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    NSLog(@"Error while playing sound %@", error);
}

- (IBAction)playSong:(id)sender {
    NSIndexPath* indexPath = [NSIndexPath indexPathForItem:self.selectedRow inSection:0];
    self.audioPlayer.selectedRow = indexPath;
    [self.audioPlayer playSong:self.playButton];
}

- (IBAction)timeToSeek:(id)sender {
    NSIndexPath* indexPath = [NSIndexPath indexPathForItem:self.selectedRow inSection:0];
    self.audioPlayer.selectedRow = indexPath;
    [self.audioPlayer timeToSeek:self.currentTimeAudioSlider];
}

- (IBAction)nextSong:(id)sender {
    if(self.selectedRow+1 <= [self.musiques count]){
        self.selectedRow += 1;
        NSIndexPath* indexPath = [NSIndexPath indexPathForItem:self.selectedRow inSection:0];
        self.audioPlayer.selectedRow = indexPath;
        [self.tableView selectRowAtIndexPath: indexPath  animated:true scrollPosition:UITableViewScrollPositionNone];
        [self.audioPlayer setSong:self.selectedRow musiques:self.musiques songNameLabel:self.songName artisteLabel:self.artiste albumCoverImageView:self.albumCover];
        if ([self.audioPlayer isPlaying]) {
            [[self.audioPlayer audioPlayer] play];
        } else {
            [self.audioPlayer playSong:self.playButton];
        }
    }
}

- (IBAction)previousSong:(id)sender {
    if(self.selectedRow-1 >= 0){
        self.selectedRow -= 1;
        NSIndexPath* indexPath = [NSIndexPath indexPathForItem:self.selectedRow inSection:0];
        self.audioPlayer.selectedRow = indexPath;
        [self.tableView selectRowAtIndexPath: indexPath animated:true scrollPosition:UITableViewScrollPositionNone];
        [self.audioPlayer setSong:self.selectedRow musiques:self.musiques songNameLabel:self.songName artisteLabel:self.artiste albumCoverImageView:self.albumCover];
        if ([self.audioPlayer isPlaying]) {
            [[self.audioPlayer audioPlayer] play];
        } else {
            [self.audioPlayer playSong:self.playButton];
        }
    }
}

- (IBAction)showMenu:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
