//
//  NouveauTestament.m
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "NouveauTestament.h"

@implementation NouveauTestament

+(NouveauTestament*) getNouveauTestamentByDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<NouveauTestament *>* bbs = [NouveauTestament objectsWithPredicate:predicate];
    return [bbs objectAtIndex:0];
}

+ (BOOL) deleteNouveauTestamentWithDamId:(NSString*) damId{
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObject:[self getNouveauTestamentByDamId:damId]];
    [realm commitWriteTransaction];
    return true;
}

+(NSMutableArray<NouveauTestament*>*) getNouveauxTestamentsByDamId:(NSString *)damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    RLMResults<NouveauTestament *>* bbs = [NouveauTestament objectsWithPredicate:predicate];
    NSMutableArray<NouveauTestament*> *atArray = [[NSMutableArray alloc] init];
    for (NouveauTestament *testament in bbs) {
        [atArray addObject:testament];
    }
    return atArray;
}
@end
