//
//  CitationImageViewController.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 11/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "CitationImageViewController.h"
#import "CitationImageViewCollectionViewCell.h"
#import "DataService.h"
#import "CitationImage.h"
#import "AFHTTPSessionManager.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "DEMONavigationController.h"
#import "UIImageView+AFNetworking.h"

@implementation CitationImageViewController

//Initialisation de la page à 0.
static int page = 0;

static NSString * const reuseIdentifier = @"CitationItemCell";

+ (int) page
{
    return page;
}

- (void) addPage
{
    page = page +1 ;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Citations en image";
    
    //Initialisation de la liste des citations.
    self.citationItems = [[NSMutableArray alloc] init];
    
    [self loadMoreCitationImage:page];
    [self.collectionView setDelegate:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.citationItems.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CitationImageViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    //Index de la cellule
    NSInteger row = indexPath.row;
    
    //Initialisation de la cellule avec les paramètres de la citation.
    CitationImage* citationImage = [self.citationItems objectAtIndex:row];
    cell.title.text = citationImage.title;
    cell.like_control.objectID = cell.like_control.objectID = [NSString stringWithFormat:@"%@", citationImage.facebook_link_html];
    cell.publish_date.text = citationImage.publish_up;
    

    /*
     Requete asynchrone pour le chargement d'image,
     afin d'afficher un loader pendant le telechargement de celle ci.
     */
    cell.indicator.hidesWhenStopped = true;
    [cell.thumbnail setImageWithURLRequest: [NSURLRequest requestWithURL:citationImage.thumbnail] placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [cell.indicator stopAnimating];
        [cell.thumbnail setImage:image];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [cell.indicator stopAnimating];
    }];
    return cell;
}

/*!
 * @discussion Displaying cells
 * @param UICollectionViewCell, NSIndexPath
 * @return void
 */
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0.6;
    
    cell.layer.transform = self.animation;
    cell.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int h2 = scrollView.contentSize.height - scrollView.frame.size.height;
    int h1 = scrollView.contentOffset.y;
    NSLog(@"h1 : %d - h2 : %d",h1,h2);
    if (scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height))
    {
        //Charger plus d'articles quand on atteint le bas de la page.
        [self loadMoreCitationImage:page];
    }
    
    
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(1.1, 1.1, 0);
        self.animation = CATransform3DTranslate(scale, 0, -70, 0);
        NSLog(@"Scrolling Up");
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(0.9, 0.9, 0);
        self.animation = CATransform3DTranslate(scale, 0, 70, 0);
        NSLog(@"Scrolling Down");
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

/*!
 * @discussion set cell fullScreenSize
 * @param (UICollectionView *)collectionView
 * @param (UICollectionViewLayout *)collectionViewLayout
 * @param (NSIndexPath *)indexPath
 * @return CGSize
 */
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGFloat width = self.view.bounds.size.width/2.4;
        return CGSizeMake(width, width/1.95);
    }
    else {
        CGFloat width = self.view.bounds.size.width;
        return CGSizeMake(width, width/1.95);
    }
}

-(void)loadMoreCitationImage:(int)page
{
    DataService* dataService = [DataService sharedDataService];
    
    if([dataService canConnectToWeb])
    {
        
        NSString* url = @"http://www.adoredieu.com/mobility/citation_images.php?items_per_page=10&page=";
        url = [url stringByAppendingString:[NSString stringWithFormat:@"%d", page]];
        [self addPage];
        /*
         Requete JSON, et parsing de la réponse json reçue.
         */
        
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:url
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if([responseObject isKindOfClass:[NSArray class]])
                 {
                     //Récuperation du premier tableau JSON dans arr.
                     NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
                     
                     for(NSDictionary* dictionnary in arr)
                     {
                         CitationImage* citation = [[CitationImage alloc] init];
                         citation.Id = [dictionnary objectForKey:@"id"];
                         citation.publish_up = [dictionnary objectForKey:@"publish_up"];
                         citation.article_link = [dictionnary objectForKey:@"article_link"];
                         citation.title = [dictionnary objectForKey:@"title"];
                         citation.introtex = [dictionnary objectForKey:@"introtext"];
                         citation.thumbnail = [NSURL URLWithString:[[dictionnary objectForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]];
                         [self.citationItems addObject:citation];
                     }
                     
                     NSLog(@"JSON: %@", responseObject);
                 }
                 [self.collectionView reloadData];
             }
             failure:^(NSURLSessionDataTask *task, id responseObject) {
                 NSLog(@"JSON: %@", responseObject);
             }];
    }
}
- (IBAction)showMenu:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

@end
