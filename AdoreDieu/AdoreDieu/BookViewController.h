//
//  BookViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 14/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBT.h" 
#import "DBTChapter.h"

@interface BookViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>


@property (weak, nonatomic) IBOutlet UIPickerView *bookPickerView;
@property (weak, nonatomic) IBOutlet UITextView *content;

@property NSMutableArray<DBTBook *>* books;
@property NSMutableArray<DBTChapter *>* chapters;
@property NSMutableArray<DBTVerse *>* verses;
@property NSString* damId;
@property NSString* bookId;
@property NSMutableString* allVerses;

-(void) loadVersesDamId:(NSString *) damId
                  book:(NSString *) bookId
               chapter:(NSNumber *) chapterId;

-(void)loadChaptersWithDamId:(NSString *) damId
                           bookId:(NSString *) bookId
                          chapter:(NSNumber*) chapterId;

-(void) loadBooksWithDamId:(NSString *)damId;
@end
