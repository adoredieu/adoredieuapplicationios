//
//  NouveauTestament.h
//  AdoreDieu
//
//  Created by Jeremy on 22/11/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Realm/Realm.h>
#import "Livre.h"
@interface NouveauTestament : RLMObject

@property NSString* damId;
@property int verseNumber;
@property RLMArray<Livre*><Livre>* livres;

+(NouveauTestament*) getNouveauTestamentByDamId:(NSString *)damId;
+ (BOOL) deleteNouveauTestamentWithDamId:(NSString*) damId;
+(NSMutableArray<NouveauTestament*>*) getNouveauxTestamentsByDamId:(NSString *)damId;

@end
