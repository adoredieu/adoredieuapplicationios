//
//  PenseeDetailViewController.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 12/07/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "PenseeDetailViewController.h"
#import "DataService.h"
#import "AFNetworking.h"
#import "PenseeDetailViewController.h"
#import "DEMONavigationController.h"
#import <KVNProgress/KVNProgress.h>

@interface PenseeDetailViewController ()

@end

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


@implementation PenseeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.oldHeight = 0;
    
    // Title
    self.title = @"Pensée du jour";
    
    // Do any additional setup after loading the view.
    [self loadPensee];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    ScrollDirection scrollDirection;
    
    if (self.oldHeight == 0){
        self.oldHeight = self.content.frame.size.height;
    }
    
    if (self.lastContentOffSet > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
        [UIView animateWithDuration:.5 animations:^{
            //set frame of bottom view to bottom of screen (show 60%)
            self.headerView.frame = CGRectMake(0, 64, self.headerView.frame.size.width, 178);
            self.content.frame = CGRectMake(0, 64+178, self.content.frame.size.width, self.oldHeight);
        }];
        NSLog(@"Did scroll to top");
    }
    else if (self.lastContentOffSet < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
        [UIView animateWithDuration:.5 animations:^{
            //set frame of bottom view to bottom of screen (show 60%)
            self.headerView.frame = CGRectMake(0, 64 - 178, self.headerView.frame.size.width, 178);
            self.content.frame = CGRectMake(0, 64, self.content.frame.size.width, self.oldHeight+178);
        }];
        NSLog(@"Did scroll to Bottom");
    }
    
    self.lastContentOffSet = scrollView.contentOffset.y;
}

-(void)loadPensee
{
    DataService* dataService = [DataService sharedDataService];
    if([dataService canConnectToWeb])
    {
        
        NSString* url = @"http://www.adoredieu.com/mobility/pensee_details_v2.php?id=";
        url = [url stringByAppendingString:[NSString stringWithFormat:@"%@", self.Id]];

        /*
         Requete JSON, et parsing de la réponse json reçue.
         */
        
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:url
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 
                 NSLog(@"JSON: %@", responseObject);
                 NSString* content = [responseObject objectForKey:@"content"];
                 
                 NSError* error = nil;
                 
                 //Formattage du texte
                 NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<[^>]*>" options:NSRegularExpressionCaseInsensitive error:&error];
                 
                 NSString *modifiedString = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, [content length]) withTemplate:@""];
                 
                 modifiedString = [modifiedString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
                 
                 self.content.text = modifiedString;
                 self.publish_date.text = [responseObject objectForKey:@"publish_up"];
                 self.titre.text = [responseObject objectForKey:@"title"];
                 self.fb_article_link.objectID = [responseObject objectForKey:@"fb_article_link"];
                 
                 /*
                  Requete asynchrone pour le chargement d'image,
                  afin d'afficher un loader pendant le telechargement de celle ci.
                  */
                 dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
                 dispatch_async(downloadQueue, ^{
                     UIImage* image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.thumbnail]];                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self.image setImage:image];
                         [self.imgIndicator stopAnimating];
                     });
                 });
             }
             failure:^(NSURLSessionDataTask *task, id responseObject) {
                 [KVNProgress showErrorWithStatus:@"Pas de connexion detectée"];
             }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
