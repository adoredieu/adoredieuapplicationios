//
//  Enseignement.h
//  AdoreDieu
//
//  Created by Jeremy on 11/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Enseignement : NSObject

@property NSString* Id;
@property NSString* publish_up;
@property NSString* article_link;
@property NSString* title;
@property NSString* introtex;
@property NSURL* thumbnail;

@end
