//
//  DEMOHomeViewController.h
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "AFHTTPSessionManager.h"
#import "Pensees.h"
#import "UIImageView+AFNetworking.h"
#import "Enseignement.h"

@interface HomeViewController : UIViewController


- (IBAction)showMenu;
@property (weak, nonatomic) IBOutlet UILabel *citationLabel;

@property (weak, nonatomic) IBOutlet UILabel *verseLabel;
@property UIView* progress;

- (instancetype)initWithCoder:(NSCoder *)aDecoder;
@end
