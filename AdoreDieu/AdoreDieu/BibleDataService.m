//
//  BibleDataServce.m
//  AdoreDieu
//
//  Created by Jeremy on 12/10/2015.
//  Copyright © 2015 AdoreDieu. All rights reserved.
//

#import "BibleDataService.h"
#import "DBT.h"
#import "Livre.h"
#import <KVNProgress/KVNProgress.h>

@implementation BibleDataService


// Download bible
- (void) downloadBibleWithDamId:(NSString*) damId{
    
        //Initialisation du DataService pour vérifier la présence d'une connexion
        DataService* dataService = [DataService sharedDataService];
    
        //[KVNProgress showProgress:0.0f status:@"Téléchargement de la bible"];
    
    @try{
        
        //Récupération du livre (AT ou NT)
        if([dataService canConnectToWeb])
        {
            self.vMaxNumber = 100000;
            self.verseNumber = 1 ;
                [DBT getLibraryVolumeWithDamID:damId
                                  languageCode:@"FRN"
                                         media:@"text"
                                       success:^(NSArray *volumes) {
                                               if([volumes isKindOfClass:[NSArray class]])
                                               {
                                                   for(DBTVolume* v in volumes)
                                                   {
                                                       
                                                       NSString* bibleDamId = [v.damId substringToIndex:6];
                                                        NSLog(@"Thread 1 : %@", [NSThread currentThread]);
                                                       //Création d'un prédicat pour vérifier l'existence de la bible
                                                       //dans la base de données.
                                                           dispatch_async(self.queueToRunBDS,^{
                                                           RLMRealm *realm = [RLMRealm defaultRealm];
                                                           self.bibles = [Bible allObjects];
                                                           NSLog(@"Initial thread : %@", [NSThread currentThread]);
                                                           NSPredicate* predicate = [NSPredicate predicateWithFormat:@"versionCode = %@",v.versionCode];
                                                          //Recherche d'une bible avec le prédicat
                                                           NSLog(@"Thread 2 : %@", [NSThread currentThread]);
                                                           RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
                                                       
                                                               if([bbs count] > 0)
                                                               {
                                                                   for(Bible* b in bbs)
                                                                   {
                                                                       NSLog(@" ---------- La bible existe deja --------------- ");
                                                                       if([v.collectionCode  isEqual: @"OT"] && b.at == NULL)
                                                                       {
                                                                           // Création de de l'objet ancien testament
                                                                           AncienTestament* ancientTestament = [[AncienTestament alloc] init];
                                                                           ancientTestament.damId = v.damId;
                                                                           [self setVerseNumberForAt:ancientTestament];
                                                                           [realm beginWriteTransaction];
                                                                           b.at = ancientTestament;
                                                                           [realm commitWriteTransaction];
                                                                           
                                                                           [self downloadBooksWithBibleDamId:bibleDamId originalDamId:v.damId collectionCode:@"AT" queue:self.queueToRunBDS];
                                                                       }
                                                                       else if([v.collectionCode  isEqual: @"NT"] && b.nt == NULL)
                                                                       {
                                                                           // Création de l'objet nouveau testament
                                                                           NouveauTestament* nouveauTestament = [[NouveauTestament alloc] init];
                                                                           nouveauTestament.damId = v.damId;
                                                                           [self setVerseNumberForNt:nouveauTestament];
                                                                           [realm beginWriteTransaction];
                                                                            b.nt = nouveauTestament;
                                                                            [realm commitWriteTransaction];
                                                                           [self downloadBooksWithBibleDamId:bibleDamId originalDamId:v.damId collectionCode:@"NT" queue:self.queueToRunBDS];
                                                                       }
                                                                   }
                                                               }
                                                               else{
                                                                   //TODO création d'une bible
                                                                   NSLog(@" ------------ La bible n'existe pas -------------- ");
                                                                   
                                                                   if([v.collectionCode  isEqual: @"OT"])
                                                                   {
                                                                       RLMRealm* realm = [RLMRealm defaultRealm];
                                                                       Bible* b = [[Bible alloc] init];
                                                                       b.versionCode = v.versionCode;
                                                                       b.versionName = v.versionName;
                                                                       b.damId = bibleDamId;
                                                                       // Création de de l'objet ancien testament
                                                                       AncienTestament* ancientTestament = [[AncienTestament alloc] init];
                                                                       ancientTestament.damId = v.damId;
                                                                       [self setVerseNumberForAt:ancientTestament];
                                                                       b.at = ancientTestament;

                                                                       [realm beginWriteTransaction];
                                                                       [realm addObject:b];
                                                                       [realm commitWriteTransaction];
                                                                       
                                                                       [self downloadBooksWithBibleDamId:bibleDamId originalDamId:v.damId collectionCode:@"AT" queue:self.queueToRunBDS];
                                                                   }
                                                                   else if([v.collectionCode  isEqual: @"NT"])
                                                                   {
                                                                        RLMRealm*  realm = [RLMRealm defaultRealm];
                                                                        Bible* b = [[Bible alloc] init];
                                                                        b.versionCode = v.versionCode;
                                                                        b.versionName = v.versionName;
                                                                        b.damId = bibleDamId;
                                                                        // Création de l'objet nouveau testament
                                                                        NouveauTestament* nouveauTestament = [[NouveauTestament alloc] init];
                                                                        nouveauTestament.damId = v.damId;
                                                                        [self setVerseNumberForNt:nouveauTestament];
                                                                        b.nt = nouveauTestament;
                                                                        NSLog(@"Thread 6 : %@", [NSThread currentThread]);
                                                                        [realm beginWriteTransaction];
                                                                        [realm addObject:b];
                                                                        [realm commitWriteTransaction];
                                                                       [self downloadBooksWithBibleDamId:bibleDamId originalDamId:v.damId collectionCode:@"NT" queue:self.queueToRunBDS];
                                                                   }
                                                               }
                                                           });
                                                   }
                                               }
                                       } failure:^(NSError *error) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [KVNProgress dismissWithCompletion:^{
                                                   [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
                                               }];
                                           });
                                           NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
                                           //Recherche d'une bible avec le prédicat
                                           NSLog(@"Thread 2 : %@", [NSThread currentThread]);
                                           RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
                                           RLMRealm* realm = [RLMRealm defaultRealm];
                                           [realm beginWriteTransaction];
                                           [realm deleteObject:[bbs objectAtIndex:0]];
                                           [realm commitWriteTransaction];
                                       }];
                 }
    }
    @catch(NSException* exception){
        dispatch_async(dispatch_get_main_queue(), ^{
            [KVNProgress dismissWithCompletion:^{
                [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
            }];
        });
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
        RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
        RLMRealm* realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:[bbs objectAtIndex:0]];
        [realm commitWriteTransaction];
    }
}

-(void)setVerseNumberForAt:(AncienTestament*) ancienTestament
{
        RLMRealm* realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        if ([ancienTestament.damId isEqual:@"FRNDBYO2ET"]) {
            ancienTestament.verseNumber = 22275;
        }
        if ([ancienTestament.damId isEqual:@"FRNPDCO2ET"]) {
            ancienTestament.verseNumber = 25826;
        }
        if ([ancienTestament.damId isEqual:@"FRNPDVO2ET"]) {
            ancienTestament.verseNumber = 22043;
        }
        [realm commitWriteTransaction];
}

-(void)setVerseNumberForNt:(NouveauTestament*) nouveauTestament
{
    //dispatch_async(self.queueToRunBDS, ^{
        RLMRealm* realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        if ([nouveauTestament.damId isEqual:@"FRNDBYN2ET"]) {
            nouveauTestament.verseNumber = 7357;
        }
        if ([nouveauTestament.damId isEqual:@"FRNPDCN2ET"]) {
            nouveauTestament.verseNumber = 7356;
        }
        if ([nouveauTestament.damId isEqual:@"FRNPDVN2ET"]) {
            nouveauTestament.verseNumber = 7339;
        }
        [realm commitWriteTransaction];
    //});
}



-(void)downloadBooksWithBibleDamId:(NSString *)bibleDamId originalDamId:(NSString*)damId collectionCode:(NSString*) collectionCode queue:(dispatch_queue_t)queue
{
            [DBT getLibraryBookWithDamId:damId
                                 success:^(NSArray *v_books) {
                                             for(DBTBook* book in v_books)
                                             {
                                                 dispatch_async(self.queueToRunBDS,^{
                                                     RLMRealm *r1 = [RLMRealm defaultRealm];
                                                     
                                                     Bible *b = [Bible getBibleByDamId:bibleDamId];
                                                     //Mise en place des chapitres
                                                     Livre* livre = [[Livre alloc] init];
                                                     livre.bookId = book.bookId;
                                                     livre.damId = book.damId;
                                                     livre.bookName = book.bookName;
                                                     livre.numberOfChapters = [book.numberOfChapters intValue];
                                                     
                                                     if([collectionCode isEqualToString:@"NT"])
                                                     {
                                                            NSLog(@"Thread 8 : %@", [NSThread currentThread]);
                                                            [r1 transactionWithBlock:^{
                                                                [[[b nt] livres] addObject:livre];
                                                            }];
                                                     }
                                                     else if([collectionCode isEqualToString:@"AT"])
                                                     {
                                                            NSLog(@"Thread 9 : %@", [NSThread currentThread]);
                                                             //RLMRealm* realm = [RLMRealm defaultRealm];
                                                             [r1 beginWriteTransaction];
                                                             [[[b at] livres] addObject:livre];
                                                             [r1 commitWriteTransaction];
                                                     }
                                                     
                                                     [self downloadChapterFromLivre:livre bookId:livre.bookId andDamId:damId];
                                                     
                                                 });
                                             }
                                 } failure:^(NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [KVNProgress dismissWithCompletion:^{
                                             [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
                                         }];
                                     });
                                     NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
                                     //Recherche d'une bible avec le prédicat
                                     NSLog(@"Thread 2 : %@", [NSThread currentThread]);
                                     RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
                                     RLMRealm* realm = [RLMRealm defaultRealm];
                                     [realm beginWriteTransaction];
                                     [realm deleteObject:[bbs objectAtIndex:0]];
                                     [realm commitWriteTransaction];
                                 }];
}

- (void) downloadChapterFromLivre:(Livre*)livre bookId:(NSString*)bookId andDamId:(NSString*) damId{
    dispatch_async(self.queueToRunBDS, ^{
        RLMRealm* realm = [RLMRealm defaultRealm];
        Livre* livre = [Livre getLivreByBookId:bookId andDamId:damId];
        @try {
            for(int i = 1 ; i <= livre.numberOfChapters ; i++)
            {
                Chapitre* chapitre = [[Chapitre alloc] init];
                chapitre.index = i;
                chapitre.damId = livre.damId;
                chapitre.bookId = livre.bookId;
                chapitre.chapterName = [NSString stringWithFormat:@"Chapitre %d",i];
                
                if (realm == [RLMRealm defaultRealm]) {
                    [realm beginWriteTransaction];
                    [livre.chapitres addObject:chapitre];
                    [realm commitWriteTransaction];
                }
                else{
                    realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    [livre.chapitres addObject:chapitre];
                    [realm commitWriteTransaction];
                }
                
                [self downloadVerseWithDamId:chapitre.damId bookId:livre.bookId index:i queue:self.queueToRunBDS];
            }
        }
        @catch (NSException *exception) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [KVNProgress dismissWithCompletion:^{
                    [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
                }];
            });
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",livre.damId];
            //Recherche d'une bible avec le prédicat
            NSLog(@"Thread 2 : %@", [NSThread currentThread]);
            RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
            RLMRealm* realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm deleteObject:[bbs objectAtIndex:0]];
            [realm commitWriteTransaction];
        }
    });
}

- (void) downloadVerseWithDamId:(NSString *)damId bookId:(NSString *)bookId index:(int)index queue:(dispatch_queue_t)queue{
    @try{
        self.vMaxNumber = [self returnVerseNumberForAtOrNTWithDamID:damId];
        [DBT getTextVerseWithDamId:damId
                              book:bookId
                           chapter:[NSNumber numberWithInt:index]
                        verseStart:nil
                          verseEnd:nil
                           success:^(NSArray *verses) {
                               dispatch_async(queue,^{
                                   NSLog(@"Verse 1");
                                RLMRealm* realm = [RLMRealm defaultRealm];
                                Chapitre* chapitre = [Chapitre getChapitreByIndex:index withDamId:damId andBookId:bookId];
                               [realm transactionWithBlock:^{
                                    for(DBTVerse* verse in verses)
                                    {
                                        int progressValue = ((100*self.verseNumber)/self.vMaxNumber)*1.0f;
                                        Verset* verset = [[Verset alloc] init];
                                        verset.verset = verse.verseText;
                                        verset.index = [verse.verseId intValue];
                                        verset.bookId = verse.bookId;
                                        verset.bookName = verse.bookName;
                                        verset.bookOrder = [verse.bookOrder intValue];
                                        verset.chapterId = [verse.chapterId intValue];
                                        verset.chapterTitle = verse.chapterTitle;
                                        verset.paragraphNumber = [verse.paragraphNumber intValue];
                                        verset.damId = damId;
                                        NSLog(@"Thread 11 : %@", [NSThread currentThread]);
                                        [chapitre.versets addObject:verset];
                                        
                                        float v = progressValue * 0.01;
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [KVNProgress updateProgress:v animated:NO];
                                        });
                                        
                                        self.verseNumber +=1 ;
                                        NSLog(@"%d / %d --- progress Value : %d ",self.verseNumber, self.vMaxNumber, progressValue );
                                        
                                        if(self.verseNumber >= self.vMaxNumber)
                                        {
                                            //[self updateProgressViewandSetBibleDownloaded:damId];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [KVNProgress dismissWithCompletion:^{
                                                    [KVNProgress showSuccessWithStatus:@"Bible télechargée"];
                                                }];
                                            });
                                        }
                                    }
                               }];
                               });

                           } failure:^(NSError *error)
         {
             NSLog(@"Error: %@", error);
             dispatch_async(dispatch_get_main_queue(), ^{
                 [KVNProgress dismissWithCompletion:^{
                     [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
                 }];
             });
             NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
             RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
             RLMRealm* realm = [RLMRealm defaultRealm];
             [realm beginWriteTransaction];
             [realm deleteObject:[bbs objectAtIndex:0]];
             [realm commitWriteTransaction];
         }];
    }
    @catch(NSException* e){
        dispatch_async(dispatch_get_main_queue(), ^{
            [KVNProgress dismissWithCompletion:^{
                [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
            }];
        });
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
        //Recherche d'une bible avec le prédicat
        NSLog(@"Thread 2 : %@", [NSThread currentThread]);
        RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
        RLMRealm* realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:[bbs objectAtIndex:0]];
        [realm commitWriteTransaction];
    }
}

- (int) returnVerseNumberForAtOrNTWithDamID:(NSString*)damId{
        RLMResults<AncienTestament *> *at;
        RLMResults<NouveauTestament *> *nt;
    
        at = [AncienTestament objectsWhere:@"damId = %@",damId];
        nt = [NouveauTestament objectsWhere:@"damId = %@",damId];
        
        int vNumber = 0;
        
        if([at count] > 0 && [nt count] == 0)
        {
            vNumber = [[at objectAtIndex:0] verseNumber];
        }
        if([at count] == 0 && [nt count] > 0)
        {
            vNumber = [[nt objectAtIndex:0] verseNumber];
        }
        return vNumber;
}

- (BOOL)checkIfAtOrNtIsAvailableWithDamId:(NSString*) damId{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    
    //Recherche d'une bible avec le prédicat
    RLMResults<AncienTestament *>* at = [AncienTestament objectsWithPredicate:predicate];
    RLMResults<NouveauTestament *>* nt = [NouveauTestament objectsWithPredicate:predicate];
    if([at count] > 0 || [nt count] > 0)
    {
        return true;
    }
    else{
        return false;
    }
}
- (void)updateProgressViewandSetBibleDownloaded:(NSString*)damId{
        RLMRealm* realm = [RLMRealm defaultRealm];

        //[MRProgressOverlayView dismissOverlayForView:view animated:YES];
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
        
        //Recherche d'une bible avec le prédicat
        RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
        if([bbs count] > 0)
        {
            for(Bible *b in bbs)
            {
                [realm beginWriteTransaction];
                b.downloaded = true;
                [realm commitWriteTransaction];
            }
        }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [KVNProgress dismissWithCompletion:^{
            [KVNProgress showErrorWithStatus:@"Une erreur est survenue"];
        }];
    });
    /*NSPredicate* predicate = [NSPredicate predicateWithFormat:@"damId = %@",damId];
    //Recherche d'une bible avec le prédicat
    NSLog(@"Thread 2 : %@", [NSThread currentThread]);
    RLMResults<Bible>* bbs = [self.bibles objectsWithPredicate:predicate];
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObject:[bbs objectAtIndex:0]];
    [realm commitWriteTransaction];*/
}

@end

