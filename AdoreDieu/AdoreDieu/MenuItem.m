//
//  MenuItem.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 22/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

-(id) initWithName:(NSString*)name imgName:(NSString*)imgname
{
    self = [super init];
    if(self)
    {
        self.imgName = imgname;
        self.name = name;
    }
    return self;
}
@end
