//
//  CitationCollectionView.m
//  AdoreDieu
//
//  Created by RCDSM-Jeremy Barbe on 21/05/2015.
//  Copyright (c) 2015 AdoreDieu. All rights reserved.
//

#import "PenseesCollectionView.h"
#import "PenseesCollectionViewCell.h"
#import "DataService.h"
#import "AFNetworking.h"
#import "Pensees.h"
#import "PenseeDetailViewController.h"
#import "DEMONavigationController.h"
#import "UIImageView+AFNetworking.h"

@interface PenseesCollectionView ()
@end

@implementation PenseesCollectionView

//Initialisation de la page à 0.
static int page = 0;

static NSString * const reuseIdentifier = @"PenseesItemCell";

+ (int) page
{
    return page;
}

- (void) addPage
{
    page = page +1 ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Pensées";
    
    page = 0;
    
    //Initialisation de la liste des citations.
    self.PenseesItems = [[NSMutableArray alloc] init];
    
    [self.collectionView dataSource];
    [self.collectionView delegate];
    
    [self loadMoreArticle:page];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.PenseesItems.count ;
}

/*!
 * @discussion Displaying cells
 * @param UICollectionViewCell, NSIndexPath
 * @return void
 */
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0.6;
    
    cell.layer.transform = self.animation;
    cell.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PenseesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PenseesItemCell" forIndexPath:indexPath];
    
    //Index de la cellule
    NSInteger row = indexPath.row;
    
    //Initialisation de la cellule avec les paramètres de la citation.
    Pensees* pensees = [self.PenseesItems objectAtIndex:row];
    cell.title.text = pensees.title;
    cell.introduction.text = [pensees.introtex stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""]; ;
    cell.publish_date.text = pensees.publish_up;
    
    
    [cell.imgIndicator startAnimating];
    cell.imgIndicator.hidesWhenStopped = YES;
    
    [cell.thumbnail setImageWithURLRequest: [NSURLRequest requestWithURL:pensees.thumbnail] placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [cell.thumbnail setImage:image];
        [cell.imgIndicator stopAnimating];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        cell.imgIndicator.hidesWhenStopped = YES;
        [cell.imgIndicator stopAnimating];
    }];
    
    return cell;
}

/*!
 * @discussion center cells for the collectionView
 * @param UICollectionView
 * @param UICollectionViewLayout
 * @param NSInteger
 * @return UIEdgetInsets
 */
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSInteger numberOfCells = self.view.frame.size.width / 332;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * 332)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    Pensees *p = [self.PenseesItems objectAtIndex:row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PenseeDetailViewController *viewController = (PenseeDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Pensée detail"];
    viewController.Id = p.Id;
    viewController.thumbnail = p.thumbnail;
            //[self.frostedViewController presentViewController:viewController animated:YES completion:nil];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)
    {
        //Charger plus d'articles quand on atteint le bas de la page.
        [self loadMoreArticle:page];
    }
    
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(1.1, 1.1, 0);
        self.animation = CATransform3DTranslate(scale, 0, -70, 0);
        NSLog(@"Scrolling Up");
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        CATransform3D scale;
        scale = CATransform3DMakeScale(0.9, 0.9, 0);
        self.animation = CATransform3DTranslate(scale, 0, 70, 0);
        NSLog(@"Scrolling Down");
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

-(void)loadMoreArticle:(int)page
{
    DataService* dataService = [DataService sharedDataService];
    
    if([dataService canConnectToWeb])
    {
        
        NSString* url = @"http://www.adoredieu.com/mobility/pensees.php?items_per_page=10&page=";
        url = [url stringByAppendingString:[NSString stringWithFormat:@"%d", page]];
        [self addPage];
        /*
         Requete JSON, et parsing de la réponse json reçue.
         */
        
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:url
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 if([responseObject isKindOfClass:[NSArray class]])
                 {
                     //Récuperation du premier tableau JSON dans arr.
                     NSMutableArray *arr = [NSMutableArray arrayWithArray:responseObject];
                     
                     for(NSDictionary* dictionnary in arr)
                     {
                         Pensees* pensees = [[Pensees alloc] init];
                         pensees.Id = [dictionnary objectForKey:@"id"];
                         pensees.publish_up = [dictionnary objectForKey:@"publish_up"];
                         pensees.article_link = [dictionnary objectForKey:@"article_link"];
                         pensees.title = [dictionnary objectForKey:@"title"];
                         pensees.introtex = [dictionnary objectForKey:@"introtext"];
                         pensees.thumbnail = [NSURL URLWithString:[[dictionnary objectForKey:@"thumbnail"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]];
                         [self.PenseesItems addObject:pensees];
                     }
                     
                     NSLog(@"JSON: %@", responseObject);
                 }
                 [self.collectionView reloadData];
             }
             failure:^(NSURLSessionDataTask *task, id responseObject) {
                 NSLog(@"JSON: %@", responseObject);
             }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Pensée detail segue"])
    {
        NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        // Get reference to the destination view controller
        PenseeDetailViewController *vc = [segue destinationViewController];
        
        //Index de la cellule
        NSInteger row = selectedIndexPath.row;
        
        //Initialisation de la cellule avec les paramètres de la citation.
        Pensees* p = [self.PenseesItems objectAtIndex:row];
        
        // Pass any objects to the view controller here, like...
        vc.Id = p.Id;
        vc.thumbnail = p.thumbnail;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = self.view.bounds.size.width;
    return CGSizeMake(width, width*1.3);
}

- (IBAction)showMenu:(id)sender {
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


#pragma mark - Navigation


@end
