//
//  CustomNotifierRest.m
//  AdoreDieu
//
//  Created by Jeremy on 18/03/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "CustomNotifierRest.h"

@implementation CustomNotifierRest

-(BOOL) checkDateRequestWithUrl:(NSString*) url andUserPreference:(NSDate*) date{
    //Configuration du session manager
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    __block BOOL available = true;
    
    if(date != nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        //Execution get request pour récupérer dernière date du dernier enseignement
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSString* response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSDate *dateToCompare = [dateFormatter dateFromString:response];
            //On verifie si la date est nouvelle
            if ([date compare:dateToCompare] == NSOrderedAscending) {
                available = true;
            }
            else {
                available = false;
            }
            dispatch_semaphore_signal(semaphore);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            dispatch_semaphore_signal(semaphore);
            NSLog(@"Error: %@", error);
        }];
    } else {
        dispatch_semaphore_signal(semaphore);
    }
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    return available;
}

@end
