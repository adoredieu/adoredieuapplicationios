//
//  BooksViewController.h
//  AdoreDieu
//
//  Created by Jeremy on 04/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Livre.h"
@interface BooksViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSMutableArray<Livre*>* livres;
@property BOOL haveToDownload;
@property NSString* damId;

@end
