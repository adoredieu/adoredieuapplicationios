//
//  BooksViewController.m
//  AdoreDieu
//
//  Created by Jeremy on 04/01/2016.
//  Copyright © 2016 AdoreDieu. All rights reserved.
//

#import "BooksViewController.h"
#import "DBT.h"
#import <KVNProgress/KVNProgress.h>
#import "ChapitresCollectionViewController.h"
#import "DBTUtils.h"
#import "BookViewTableViewCell.h"
@interface BooksViewController ()

@end

@implementation BooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.livres == nil){
        self.livres = [[NSMutableArray alloc] init];
    }
    
    self.title = @"Livres";
    
    if (self.haveToDownload == YES) {
        
        [KVNProgress showWithStatus:@"Veuillez patienter..."];
        //Téléchargement des livres
        [DBT getLibraryBookWithDamId:self.damId success:^(NSArray* bookorder){
            
            for (DBTBook* book in bookorder) {
                Livre* livre = [[Livre alloc] init];
                livre.bookId = book.bookId;
                livre.bookName = book.bookName;
                livre.damId = book.damId;
                livre.numberOfChapters = [book.numberOfChapters intValue];
                [self.livres addObject:livre];
            }
            [self.tableView reloadData];
            
        }failure:^(NSError* error){
            NSLog(@"Error while %@",error);
        }];
        [KVNProgress dismiss];
    }
    else{
        
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.livres count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BookViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"livrecell" forIndexPath:indexPath];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    cell.selectedBackgroundView = blurEffectView;
    cell.backgroundColor = [UIColor clearColor];
    
    cell.title.text = [[self.livres objectAtIndex:indexPath.row] bookName];
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    ChapitresCollectionViewController* vc = segue.destinationViewController;
    vc.livre = [self.livres objectAtIndex:indexPath.row];
}


@end
